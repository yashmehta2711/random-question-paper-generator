
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import java.util.Random;
import java.util.Vector;
import javax.swing.DefaultListModel;
import java.io.FileOutputStream;
import java.util.Date;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.Document;
import com.itextpdf.text.Font;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author yashm
 */
public class GeneratePaper extends javax.swing.JPanel {

    /**
     * Creates new form GeneratePaper
     */
    GeneratePaper gp;
    Connection conn = null;
    Statement statement = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    Random random = new Random();
    //START FOR PDF
    private static String FILE = "C:/Users/yashm/Desktop/qp.pdf";
    private static Font headingFont = new Font(Font.FontFamily.TIMES_ROMAN, 18,Font.BOLD);
    private static Font questionFont = new Font(Font.FontFamily.TIMES_ROMAN, 10);
    private static Font subHeadingFont = new Font(Font.FontFamily.TIMES_ROMAN, 12,Font.BOLD);
    //END FOR PDF
    public GeneratePaper() {
        initComponents();
        this.gp = this;
        conn = MySQLConnect.connectDB();
        fillComboSName();
        
    }
    //START FOR PDF
    private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }
    //END FOR PDF
    private void fillListlChapter(){
        int id = 0;
        try{
            String sname = cmbSubject.getSelectedItem().toString();
            String sqlid = "Select id from subject where subject_name = '" + sname + "'";
            ps = conn.prepareStatement(sqlid);
            rs = ps.executeQuery();
            while(rs.next()){
                int idTemp = rs.getInt("id");
                id = idTemp;
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        DefaultListModel m = new DefaultListModel();
        try{
            String sql = "select * from chapter where subject_id = '" + id + "'";
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
                String cname = rs.getString("chapter_name");
                m.addElement(cname);
            }
            lChapter.setModel(m);
        }catch(Exception e){
           JOptionPane.showMessageDialog(this, "Error! " + e); 
        }
    }
    
    private void fillComboSName(){
        try{
            String sql = "select * from subject";
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            cmbSubject.addItem("Select an item");
            while(rs.next()){
                String sname = rs.getString("subject_name");
                cmbSubject.addItem(sname);
            }
            cmbSubject.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e) {
                    fillListlChapter();
                }
            });
        }catch(Exception e){
           JOptionPane.showMessageDialog(this, "Error! " + e); 
        }
    }
    
    private void getQuestions(){
        //FOR CHAPTERS Name
        int i;
        String ch1 = "";
        for(i=0; i<lChapter.getSelectedValuesList().size(); i++){
            if(i!=lChapter.getSelectedValuesList().size()-1)
                ch1 += "'" + lChapter.getSelectedValuesList().get(i) + "',";
            else
                ch1 += "'" + lChapter.getSelectedValuesList().get(i) + "'";
        }
        //For chapters id
        String sel = "";
        try{
            Object oarray[] = lChapter.getSelectedValues();
            Vector v = new Vector();
            for (Object oarray1 : oarray) {
                String sqlcid = "select id from chapter where chapter_name = '" + oarray1 + "'";
                ps = conn.prepareStatement(sqlcid);
                rs = ps.executeQuery(); 
                if(rs.next())
                    v.addElement(rs.getInt(1));
            }
            int len = v.toString().length();
            sel = v.toString().substring(1,len-1);
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        //FOR SUBJECTS
        int id = 0;
        try{
            String sname = cmbSubject.getSelectedItem().toString();
            String sqlid = "Select id from subject where subject_name = '" + sname + "'";
            ps = conn.prepareStatement(sqlid);
            rs = ps.executeQuery();
            while(rs.next()){
                int idTemp = rs.getInt("id");
                id = idTemp;
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        
        
        String selectedDiff = cmbDiff.getSelectedItem().toString();
        String selectedSubject = cmbSubject.getSelectedItem().toString();
        String selected4Marks = cmb4Marks.getSelectedItem().toString();
        String selected8Marks = cmb8Marks.getSelectedItem().toString();
        String selected10Marks = cmb10Marks.getSelectedItem().toString();
        int Marks4 = Integer.parseInt(selected4Marks);
        int Marks8 = Integer.parseInt(selected8Marks);
        int Marks10 = Integer.parseInt(selected10Marks);
        int no;
        int gotQ4 = 0, gotQ8 = 0, gotQ10 = 0;
        
        
        //Output
        String line1 = "Subject Name : " + selectedSubject;
        String line2 = "Chapters : " + ch1;
        String line3 = "Total Marks : " + txtTM.getText();
        String line4 = "Duration : " + txtDuration.getText();
        System.out.println(line1);System.out.println(line2);System.out.println(line3);System.out.println(line4);
        
        //4 Marks
        try{
            ArrayList<String> al4 = new ArrayList<>();
            ArrayList<String> al4m = new ArrayList<>();
            ArrayList<Integer> al4o = new ArrayList<>();
            String sql = "select question,weightage,occurence from question where weightage = 4 and subject_id = '" + id + "' and difficulty = '" + selectedDiff + "' and chapter_id IN (" + sel + ") order by occurence LIMIT " + Marks4 + ";";            
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
                String question = rs.getString("question");
                String m = rs.getString("weightage");
                int o = rs.getInt("occurence");
                al4.add(question);
                al4m.add(m);
                al4o.add(o);
//                    System.out.println(al4);
            }
            if(Marks4!=0){
                if(al4.isEmpty()){
                    JOptionPane.showMessageDialog(this, "There are no enough 4 marks question from the selected chapters! Please retry");
                }else{
                    ArrayList<Integer> al4R = new ArrayList<Integer>();
                    String line5 = "Q1. 4 Marks Questions";
                    System.out.println(line5);
                    while(Marks4!=gotQ4){
                        no = random.nextInt(al4.size());
                        if(al4R.contains(no)){
                            continue;
                        }else{
                            String quest = al4.get(no);
                            String marks = al4m.get(no);
                            int occurence = al4o.get(no);
                            System.out.println(quest);
                            System.out.println(marks);
                            
                            //start for pdf
                                try {
                                    Document document = new Document();
                                    PdfWriter.getInstance(document, new FileOutputStream(FILE));
                                    document.open();
                                    Paragraph preface = new Paragraph();
                                    addEmptyLine(preface, 1);
                                    preface.add(new Paragraph("Random Question Paper", headingFont));
                                    addEmptyLine(preface, 1);
                                    preface.add(new Paragraph("Question Paper generated by: " + "Yash Shirish Mehta" + ", " + new Date(), subHeadingFont));
                                    preface.add(new Paragraph(line1,subHeadingFont));
                                    preface.add(new Paragraph(line2,subHeadingFont));
                                    preface.add(new Paragraph(line3,subHeadingFont));
                                    preface.add(new Paragraph(line4,subHeadingFont));
                                    preface.add(new Paragraph(line5,subHeadingFont));
                                    
                                    preface.add(new Paragraph(quest,questionFont));
                                    preface.add(new Paragraph(marks,questionFont));
                                    document.add(preface);
                                    document.close();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            //end for pdf
                                
                            //start for occurence
                            int qid = 0;
                            String sqlq = "select id from question where question = '" + quest + "'";
                            try{
                                ps = conn.prepareStatement(sqlq);
                                rs = ps.executeQuery();
                                while(rs.next()){
                                    int qidtemp = rs.getInt("id");
                                    qid = qidtemp;
                                }
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            int ocdb = occurence + 1;
                            String sqloc = "update question set occurence = ? where id = '" + qid + "'";
                            try{
                                ps = conn.prepareStatement(sqloc);
                                ps.setInt(1, ocdb);
                                ps.execute();
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            //end for occurence
                            gotQ4++;
                            al4R.add(no);
                        }
                    }
                }
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
//         //8 Marks
        try{
            
            ArrayList<String> al8 = new ArrayList<>();
            ArrayList<String> al8m = new ArrayList<>();
            ArrayList<Integer> al8o = new ArrayList<>();
            String sql = "select question,weightage,occurence from question where weightage = 8 and subject_id = '" + id + "' and difficulty = '" + selectedDiff + "' and chapter_id IN (" + sel + ") order by occurence LIMIT " + Marks8 + ";";
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
                String question = rs.getString("question");
                String m = rs.getString("weightage");
                int o = rs.getInt("occurence");
                al8.add(question);
                al8m.add(m);
                al8o.add(o);
//                    System.out.println(al8);
            }
            if(Marks8!=0){
                if(al8.isEmpty()){
                    JOptionPane.showMessageDialog(this, "There are no enough 8 marks question from the selected chapters! Please retry");
                }else{
                    ArrayList<Integer> al8R = new ArrayList<Integer>();
                    System.out.println("Q2. 8 Marks Questions");
                    while(Marks8!=gotQ8){
                        no = random.nextInt(al8.size());
                        if(al8R.contains(no)){
                            continue;
                        }else{
                            String quest = al8.get(no);
                            String marks = al8m.get(no);
                            int occurence = al8o.get(no);
                            System.out.println(quest);
                            System.out.println(marks);
                            int qid = 0;
                            String sqlq = "select id from question where question = '" + quest + "'";
                            try{
                                ps = conn.prepareStatement(sqlq);
                                rs = ps.executeQuery();
                                while(rs.next()){
                                    int qidtemp = rs.getInt("id");
                                    qid = qidtemp;
                                }
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            int ocdb = occurence + 1;
                            String sqloc = "update question set occurence = ? where id = '" + qid + "'";
                            try{
                                ps = conn.prepareStatement(sqloc);
                                ps.setInt(1, ocdb);
                                ps.execute();
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            gotQ8++;
                            al8R.add(no);
                        }
                    }
                }
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
//        //10 Marks
        try{
            ArrayList<String> al10 = new ArrayList<>();
            ArrayList<String> al10m = new ArrayList<>();
            ArrayList<Integer> al10o = new ArrayList<>();
            String sql = "select question,weightage,occurence from question where weightage = 10 and subject_id = '" + id + "' and difficulty = '" + selectedDiff + "' and chapter_id IN (" + sel + ") order by occurence LIMIT " + Marks10 + ";";
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
                String question = rs.getString("question");
                String m = rs.getString("weightage");
                int o = rs.getInt("occurence");
                al10.add(question);
                al10m.add(m);
                al10o.add(o);
//                    System.out.println(al8);
            }
            if(Marks10!=0){
                if(al10.isEmpty()){
                    JOptionPane.showMessageDialog(this, "There are no enough 8 marks question from the selected chapters! Please retry");
                }else{
                    ArrayList<Integer> al10R = new ArrayList<Integer>();
                    System.out.println("Q2. 10 Marks Questions");
                    while(Marks10!=gotQ10){
                        no = random.nextInt(al10.size());
                        if(al10R.contains(no)){
                            continue;
                        }else{
                            String quest = al10.get(no);
                            String marks = al10m.get(no);
                            int occurence = al10o.get(no);
                            System.out.println(quest);
                            System.out.println(marks);
                            int qid = 0;
                            String sqlq = "select id from question where question = '" + quest + "'";
                            try{
                                ps = conn.prepareStatement(sqlq);
                                rs = ps.executeQuery();
                                if(rs.next()){
                                    int qidtemp = rs.getInt("id");
                                    qid = qidtemp;
                                }
//                                System.out.println(qid);
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "1Error! " + e);
                            }
                            int ocdb = occurence + 1;
                            String sqloc = "update question set occurence = ? where id = '" + qid + "'";
                            try{
                                ps = conn.prepareStatement(sqloc);
                                ps.setInt(1, ocdb);
                                ps.execute();
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            gotQ10++;
                            al10R.add(no);
                        }
                    }
                }
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        cmbDiff = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        txtTM = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        cmb4Marks = new javax.swing.JComboBox<>();
        cmb8Marks = new javax.swing.JComboBox<>();
        cmb10Marks = new javax.swing.JComboBox<>();
        btnSubmit = new javax.swing.JButton();
        cmbSubject = new javax.swing.JComboBox<>();
        jLabel8 = new javax.swing.JLabel();
        txtDuration = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        lChapter = new javax.swing.JList<>();
        jLabel9 = new javax.swing.JLabel();

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Generate Paper Customized", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Monospaced", 1, 24))); // NOI18N

        jLabel1.setFont(new java.awt.Font("Monospaced", 1, 18)); // NOI18N
        jLabel1.setText("Subject");

        jLabel2.setFont(new java.awt.Font("Monospaced", 1, 18)); // NOI18N
        jLabel2.setText("Chapter");

        jLabel3.setFont(new java.awt.Font("Monospaced", 1, 18)); // NOI18N
        jLabel3.setText("Difficulty");

        cmbDiff.setFont(new java.awt.Font("Monospaced", 0, 18)); // NOI18N
        cmbDiff.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Easy", "Medium ", "Hard" }));

        jLabel4.setFont(new java.awt.Font("Monospaced", 1, 18)); // NOI18N
        jLabel4.setText("Total Marks");

        txtTM.setFont(new java.awt.Font("Monospaced", 0, 18)); // NOI18N

        jLabel5.setFont(new java.awt.Font("Monospaced", 1, 18)); // NOI18N
        jLabel5.setText("8 Marks");

        jLabel6.setFont(new java.awt.Font("Monospaced", 1, 18)); // NOI18N
        jLabel6.setText("4 Marks");

        jLabel7.setFont(new java.awt.Font("Monospaced", 1, 18)); // NOI18N
        jLabel7.setText("10 Marks");

        cmb4Marks.setFont(new java.awt.Font("Monospaced", 0, 18)); // NOI18N
        cmb4Marks.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }));

        cmb8Marks.setFont(new java.awt.Font("Monospaced", 0, 18)); // NOI18N
        cmb8Marks.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }));

        cmb10Marks.setFont(new java.awt.Font("Monospaced", 0, 18)); // NOI18N
        cmb10Marks.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }));

        btnSubmit.setFont(new java.awt.Font("Monospaced", 1, 24)); // NOI18N
        btnSubmit.setText("Submit");
        btnSubmit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSubmitActionPerformed(evt);
            }
        });

        cmbSubject.setFont(new java.awt.Font("Monospaced", 0, 18)); // NOI18N

        jLabel8.setFont(new java.awt.Font("Monospaced", 1, 18)); // NOI18N
        jLabel8.setText("Duration");

        txtDuration.setFont(new java.awt.Font("Monospaced", 0, 18)); // NOI18N

        lChapter.setFont(new java.awt.Font("Monospaced", 0, 18)); // NOI18N
        lChapter.setMaximumSize(new java.awt.Dimension(140, 130));
        jScrollPane1.setViewportView(lChapter);

        jLabel9.setFont(new java.awt.Font("Monospaced", 1, 18)); // NOI18N
        jLabel9.setText("Note : Sufficient Questions Should Be Inserted.");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(cmb4Marks, 0, 140, Short.MAX_VALUE)
                                .addComponent(txtDuration))
                            .addComponent(cmbSubject, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(47, 47, 47)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cmb8Marks, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(57, 57, 57)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addGap(18, 18, 18)
                                .addComponent(cmbDiff, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(cmb10Marks, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(jLabel4)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(txtTM, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addContainerGap(24, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addComponent(jLabel6)
                        .addGap(188, 188, 188)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel7)
                        .addGap(63, 63, 63))))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(412, 412, 412)
                        .addComponent(btnSubmit))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(185, 185, 185)
                        .addComponent(jLabel9)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(46, 46, 46)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel3)
                                    .addComponent(cmbDiff, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(41, 41, 41)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel4)
                                    .addComponent(txtTM, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtDuration, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel8)))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel1)
                                .addComponent(jLabel2)
                                .addComponent(cmbSubject, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(57, 57, 57))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel6)
                        .addComponent(jLabel7)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmb4Marks, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmb8Marks, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmb10Marks, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 43, Short.MAX_VALUE)
                .addComponent(btnSubmit)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel9)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnSubmitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSubmitActionPerformed
        // TODO add your handling code here:
        getQuestions();
    }//GEN-LAST:event_btnSubmitActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSubmit;
    private javax.swing.JComboBox<String> cmb10Marks;
    private javax.swing.JComboBox<String> cmb4Marks;
    private javax.swing.JComboBox<String> cmb8Marks;
    private javax.swing.JComboBox<String> cmbDiff;
    private javax.swing.JComboBox<String> cmbSubject;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JList<String> lChapter;
    private javax.swing.JTextField txtDuration;
    private javax.swing.JTextField txtTM;
    // End of variables declaration//GEN-END:variables
}
