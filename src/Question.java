
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author yashm
 */
public class Question extends javax.swing.JPanel {

    /**
     * Creates new form Question
     */
    Connection conn = null;
    Statement statement = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    Question q;
    public Question() {
        initComponents();
        this.q = this;
        conn = MySQLConnect.connectDB();
        fillComboSName();
        btnAdd.setEnabled(false);
    }
    
    private void fillComboSName(){
        cmbSN.removeAllItems();
        try{
            String sql = "select * from subject";
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            cmbSN.addItem("Select an item");
            while(rs.next()){
                String sname = rs.getString("subject_name");
                cmbSN.addItem(sname);
            }
            cmbSN.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e) {
                    fillComboCName();
                }
            });
            
        }catch(Exception e){
           JOptionPane.showMessageDialog(this, "Error! " + e); 
        }
    }
    
    private void fillComboCName(){
        String sid = "";
        cmbCN.removeAllItems();
        String sname = cmbSN.getSelectedItem().toString();
        String sqlid = "Select id from subject where subject_name = '" + sname + "'";
        try{
            ps = conn.prepareStatement(sqlid);
            rs = ps.executeQuery();
            while(rs.next()){
                String idtemp = rs.getString("id");
                sid = idtemp;
            }
             
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        try{
            String sql = "select chapter_name from chapter where subject_id = '" + sid + "'";
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            cmbCN.addItem("Select an item");
            while(rs.next()){
                String cname = rs.getString("chapter_name");
                cmbCN.addItem(cname);
            }
        }catch(Exception e){
           JOptionPane.showMessageDialog(this, "Error! " + e); 
        }
    }
    
    private void clearFields(){
        txtNo.setText("");
        cmbW.setSelectedIndex(0);
        cmbD.setSelectedIndex(0);
        taQ.setText("");
    }
    
    private void insertRecord(){
        int subjectid = 0, chapterid = 0;
        try{
            String sname = cmbSN.getSelectedItem().toString();
            String sqlsid = "Select id from subject where subject_name = '" + sname + "'";
            ps = conn.prepareStatement(sqlsid);
            rs = ps.executeQuery();
            while(rs.next()){
                int idTemp = rs.getInt("id");
                subjectid = idTemp;
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error while Inserting Record!" + e.getMessage());
        }
        try{
            String cname = cmbCN.getSelectedItem().toString();
            String sqlcid = "Select id from chapter where chapter_name = '" + cname + "'";
            ps = conn.prepareStatement(sqlcid);
            rs = ps.executeQuery();
            while(rs.next()){
                int idTemp = rs.getInt("id");
                chapterid = idTemp;
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error while Inserting Record!" + e.getMessage());
        }
        try{
            String sql = "INSERT INTO question (subject_id, chapter_id, question, weightage, difficulty, occurence) VALUES (?, ?, ?, ?, ?, ?)";
            String question = taQ.getText();
            String weightage = cmbW.getSelectedItem().toString();
            String diff = cmbD.getSelectedItem().toString();
            
            ps = conn.prepareStatement(sql);
            ps.setInt(1, subjectid);
            ps.setInt(2, chapterid);
            ps.setString(3, question);
            ps.setString(4, weightage);
            ps.setString(5, diff);
            ps.setInt(6, 0);
            
            ps.execute();
            clearFields();
            JOptionPane.showMessageDialog(this, "Record Inserted Successfully!");
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error while Inserting Record!" + e.getMessage());
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        cmbSN = new javax.swing.JComboBox<>();
        cmbCN = new javax.swing.JComboBox<>();
        txtNo = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        taQ = new javax.swing.JTextArea();
        cmbD = new javax.swing.JComboBox<>();
        cmbW = new javax.swing.JComboBox<>();
        btnAdd = new javax.swing.JButton();
        btnNew = new javax.swing.JButton();

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Question", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Monospaced", 1, 24))); // NOI18N

        jLabel1.setFont(new java.awt.Font("Monospaced", 1, 18)); // NOI18N
        jLabel1.setText("Number");

        jLabel2.setFont(new java.awt.Font("Monospaced", 1, 18)); // NOI18N
        jLabel2.setText("Subject Name");

        jLabel3.setFont(new java.awt.Font("Monospaced", 1, 18)); // NOI18N
        jLabel3.setText("Chapter Name");

        jLabel4.setFont(new java.awt.Font("Monospaced", 1, 18)); // NOI18N
        jLabel4.setText("Question");

        jLabel5.setFont(new java.awt.Font("Monospaced", 1, 18)); // NOI18N
        jLabel5.setText("Weightage");

        jLabel6.setFont(new java.awt.Font("Monospaced", 1, 18)); // NOI18N
        jLabel6.setText("Difficulty");

        cmbSN.setFont(new java.awt.Font("Monospaced", 0, 18)); // NOI18N

        cmbCN.setFont(new java.awt.Font("Monospaced", 0, 18)); // NOI18N

        txtNo.setFont(new java.awt.Font("Monospaced", 0, 18)); // NOI18N

        taQ.setColumns(20);
        taQ.setFont(new java.awt.Font("Monospaced", 0, 18)); // NOI18N
        taQ.setRows(5);
        jScrollPane1.setViewportView(taQ);

        cmbD.setFont(new java.awt.Font("Monospaced", 0, 18)); // NOI18N
        cmbD.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Easy", "Medium ", "Hard" }));

        cmbW.setFont(new java.awt.Font("Monospaced", 0, 18)); // NOI18N
        cmbW.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "4", "8", "10" }));

        btnAdd.setFont(new java.awt.Font("Monospaced", 1, 24)); // NOI18N
        btnAdd.setText("Add");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        btnNew.setFont(new java.awt.Font("Monospaced", 1, 24)); // NOI18N
        btnNew.setText("New");
        btnNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel3)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(cmbCN, 0, 180, Short.MAX_VALUE))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel2)
                                .addComponent(jLabel1))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(cmbSN, 0, 180, Short.MAX_VALUE)
                                .addComponent(txtNo))))
                    .addComponent(btnNew))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(106, 106, 106)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(138, 138, 138)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cmbD, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(cmbW, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(btnAdd))))
                .addContainerGap(91, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(43, 43, 43)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(jLabel4)
                        .addComponent(txtNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(cmbSN, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(cmbW, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(85, 85, 85)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel6)
                    .addComponent(cmbCN, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 38, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAdd)
                    .addComponent(btnNew))
                .addGap(36, 36, 36))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        // TODO add your handling code here:
        insertRecord();
    }//GEN-LAST:event_btnAddActionPerformed

    private void btnNewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNewActionPerformed
        // TODO add your handling code here:
        btnAdd.setEnabled(true);
        txtNo.setEnabled(false);
        clearFields();
    }//GEN-LAST:event_btnNewActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnNew;
    private javax.swing.JComboBox<String> cmbCN;
    private javax.swing.JComboBox<String> cmbD;
    private javax.swing.JComboBox<String> cmbSN;
    private javax.swing.JComboBox<String> cmbW;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea taQ;
    private javax.swing.JTextField txtNo;
    // End of variables declaration//GEN-END:variables
}
