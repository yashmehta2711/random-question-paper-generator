
import java.awt.event.ItemEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Random;
import java.util.Vector;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author yashm
 */

/*
PATTERN
40 Marks
	pattern 1 without options 4M3Q,8M1Q,10M2Q
		4marks 3questions
		8marks 1question
		10marks 2questions
	pattern 1 with options
		4marks 4questions
		8marks 2questions
		10marks 3questions
	pattern 2 without options 4M1Q,8M2Q,10M2Q
		4marks 1question
		8marks 2questions
		10marks 2questions
	pattern 2 with options 
		4marks 2questions
		8marks 3questions
		10marks 3questions

80 Marks
	pattern 1 without options 4M6Q,8M2Q,10M4Q 
		4marks 6questions
		8marks 2questions
		10marks 4questions
	pattern 1 with options
		4marks 8questions
		8marks 4questions
		10marks 6questions
	pattern 2 without options 4M2Q,8M4Q,10M4Q
		4marks 2questions
		8marks 4questions
		10marks 4questions
	pattern 2 with options
		4marks 3questions
		8marks 6questions
		10marks 6questions

100 Marks
	pattern 1 without options 4M4Q,8M3Q,10M6Q
		4marks 4questions
		8marks 3questions
		10marks 6questions
	pattern 1 with options
		4marks 6questions
		8marks 4questions
		10marks 7questions
	pattern 2 without options 4M2Q,8M4Q,10M6Q
		4marks 2questions
		8marks 4questions
		10marks 6questions
	pattern 2 with options
		4marks 3questions
		8marks 6questions
		10marks 8questions
*/
public class GeneratePaperPreDefined extends javax.swing.JPanel {

    /**
     * Creates new form GeneratePaperPreDefined
     */
    GeneratePaperPreDefined gpp;
    Connection conn = null;
    Statement statement = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    Random random = new Random();
    String option = "";
    int operation = 0;
    public GeneratePaperPreDefined() {
        initComponents();
        this.gpp = this;
        conn = MySQLConnect.connectDB();
        fillComboSName();
    }
    
    private void fillListlChapter(){
        int id = 0;
        try{
            String sname = cmbSubject.getSelectedItem().toString();
            String sqlid = "Select id from subject where subject_name = '" + sname + "'";
            ps = conn.prepareStatement(sqlid);
            rs = ps.executeQuery();
            while(rs.next()){
                int idTemp = rs.getInt("id");
                id = idTemp;
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        DefaultListModel m = new DefaultListModel();
        try{
            String sql = "select * from chapter where subject_id = '" + id + "'";
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
                String cname = rs.getString("chapter_name");
                m.addElement(cname);
            }
            lChapter.setModel(m);
        }catch(Exception e){
           JOptionPane.showMessageDialog(this, "Error! " + e); 
        }
    }
    
    private void fillComboSName(){
        try{
            String sql = "select * from subject";
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            cmbSubject.addItem("Select an item");
            while(rs.next()){
                String sname = rs.getString("subject_name");
                cmbSubject.addItem(sname);
            }
            cmbSubject.addItemListener((ItemEvent e) -> {
                fillListlChapter();
            });
        }catch(Exception e){
           JOptionPane.showMessageDialog(this, "Error! " + e); 
        }
    }
    //40
    private void getQuestions40Pattern1WithoutOption(){
        //FOR CHAPTERS Name
        int i;
        String ch1 = "";
        for(i=0; i<lChapter.getSelectedValuesList().size(); i++){
            if(i!=lChapter.getSelectedValuesList().size()-1)
                ch1 += "'" + lChapter.getSelectedValuesList().get(i) + "',";
            else
                ch1 += "'" + lChapter.getSelectedValuesList().get(i) + "'";
        }
        //For chapters id
        String sel = "";
        try{
            Object oarray[] = lChapter.getSelectedValues();
            Vector v = new Vector();
            for (Object oarray1 : oarray) {
                String sqlcid = "select id from chapter where chapter_name = '" + oarray1 + "'";
                ps = conn.prepareStatement(sqlcid);
                rs = ps.executeQuery(); 
                if(rs.next())
                    v.addElement(rs.getInt(1));
            }
            int len = v.toString().length();
            sel = v.toString().substring(1,len-1);
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        //FOR SUBJECTS
        int id = 0;
        try{
            String sname = cmbSubject.getSelectedItem().toString();
            String sqlid = "Select id from subject where subject_name = '" + sname + "'";
            ps = conn.prepareStatement(sqlid);
            rs = ps.executeQuery();
            while(rs.next()){
                int idTemp = rs.getInt("id");
                id = idTemp;
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        
        String selectedDiff = cmbDiff.getSelectedItem().toString();
        String selectedSubject = cmbSubject.getSelectedItem().toString();
        int Marks4,Marks8,Marks10;
        Marks4 = 3;
        Marks8 = 1;
        Marks10 = 2;
        int no;
        int gotQ4 = 0, gotQ8 = 0, gotQ10 = 0;
        
        
        //Output
        System.out.println("Subject Name : " + selectedSubject);
        System.out.println("Chapters : " + ch1);
        System.out.println("Total Marks : 40");
        System.out.println("Duration : 1 Hour 30 Minutes");
        //4 Marks
        try{
            ArrayList<String> al4 = new ArrayList<>();
            ArrayList<String> al4m = new ArrayList<>();
            ArrayList<Integer> al4o = new ArrayList<>();
            String sql = "select question,weightage,occurence from question where weightage = 4 and subject_id = '" + id + "' and difficulty = '" + selectedDiff + "' and chapter_id IN (" + sel + ") ORDER BY occurence LIMIT " + Marks4 + ";";            
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
                String question = rs.getString("question");
                String m = rs.getString("weightage");
                int o = rs.getInt("occurence");
                al4.add(question);
                al4m.add(m);
                al4o.add(o);
//                    System.out.println(al4);
            }
            if(Marks4!=0){
                if(al4.isEmpty()){
                    JOptionPane.showMessageDialog(this, "There are no enough 4 marks question from the selected chapters! Please retry");
                }else{
                    ArrayList<Integer> al4R = new ArrayList<Integer>();
                    System.out.println("Q1. 4 Marks Questions");
                    while(Marks4!=gotQ4){
                        no = random.nextInt(al4.size());
                        if(al4R.contains(no)){
                            continue;
                        }else{
                            String quest = al4.get(no);
                            String marks = al4m.get(no);
                            int occurence = al4o.get(no);
                            System.out.println(quest);
                            System.out.println(marks);
                            //start for occurence
                            int qid = 0;
                            String sqlq = "select id from question where question = '" + quest + "'";
                            try{
                                ps = conn.prepareStatement(sqlq);
                                rs = ps.executeQuery();
                                while(rs.next()){
                                    int qidtemp = rs.getInt("id");
                                    qid = qidtemp;
                                }
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            int ocdb = occurence + 1;
                            String sqloc = "update question set occurence = ? where id = '" + qid + "'";
                            try{
                                ps = conn.prepareStatement(sqloc);
                                ps.setInt(1, ocdb);
                                ps.execute();
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            //end for occurence
                            gotQ4++;
                            al4R.add(no);
                        }
                    }
                }
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        //8 Marks
        try{
            ArrayList<String> al8 = new ArrayList<>();
            ArrayList<String> al8m = new ArrayList<>();
            ArrayList<Integer> al8o = new ArrayList<>();
            String sql = "select question,weightage,occurence from question where weightage = 8 and subject_id = '" + id + "' and difficulty = '" + selectedDiff + "' and chapter_id IN (" + sel + ") ORDER BY occurence LIMIT " + Marks8 + ";";            
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
                String question = rs.getString("question");
                String m = rs.getString("weightage");
                int o = rs.getInt("occurence");
                al8.add(question);
                al8m.add(m);
                al8o.add(o);
//                    System.out.println(al8);
            }
            if(Marks8!=0){
                if(al8.isEmpty()){
                    JOptionPane.showMessageDialog(this, "There are no enough 8 marks question from the selected chapters! Please retry");
                }else{
                    ArrayList<Integer> al8R = new ArrayList<Integer>();
                    System.out.println("Q2. 8 Marks Questions");
                    while(Marks8!=gotQ8){
                        no = random.nextInt(al8.size());
                        if(al8R.contains(no)){
                            continue;
                        }else{
                            String quest = al8.get(no);
                            String marks = al8m.get(no);
                            int occurence = al8o.get(no);
                            System.out.println(quest);
                            System.out.println(marks);
                            //start for occurence
                            int qid = 0;
                            String sqlq = "select id from question where question = '" + quest + "'";
                            try{
                                ps = conn.prepareStatement(sqlq);
                                rs = ps.executeQuery();
                                while(rs.next()){
                                    int qidtemp = rs.getInt("id");
                                    qid = qidtemp;
                                }
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            int ocdb = occurence + 1;
                            String sqloc = "update question set occurence = ? where id = '" + qid + "'";
                            try{
                                ps = conn.prepareStatement(sqloc);
                                ps.setInt(1, ocdb);
                                ps.execute();
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            //end for occurence
                            gotQ8++;
                            al8R.add(no);
                        }
                    }
                }
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        //10 Marks
        try{
            ArrayList<String> al10 = new ArrayList<>();
            ArrayList<String> al10m = new ArrayList<>();
            ArrayList<Integer> al10o = new ArrayList<>();
            String sql = "select question,weightage,occurence from question where weightage = 10 and subject_id = '" + id + "' and difficulty = '" + selectedDiff + "' and chapter_id IN (" + sel + ") ORDER BY occurence LIMIT " + Marks10 + ";";            
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
                String question = rs.getString("question");
                String m = rs.getString("weightage");
                int o = rs.getInt("occurence");
                al10.add(question);
                al10m.add(m);
                al10o.add(o);
//                    System.out.println(al4);
            }
            if(Marks10!=0){
                if(al10.isEmpty()){
                    JOptionPane.showMessageDialog(this, "There are no enough 10 marks question from the selected chapters! Please retry");
                }else{
                    ArrayList<Integer> al10R = new ArrayList<Integer>();
                    System.out.println("Q3. 10 Marks Questions");
                    while(Marks10!=gotQ10){
                        no = random.nextInt(al10.size());
                        if(al10R.contains(no)){
                            continue;
                        }else{
                            String quest = al10.get(no);
                            String marks = al10m.get(no);
                            int occurence = al10o.get(no);
                            System.out.println(quest);
                            System.out.println(marks);
                            //start for occurence
                            int qid = 0;
                            String sqlq = "select id from question where question = '" + quest + "'";
                            try{
                                ps = conn.prepareStatement(sqlq);
                                rs = ps.executeQuery();
                                while(rs.next()){
                                    int qidtemp = rs.getInt("id");
                                    qid = qidtemp;
                                }
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            int ocdb = occurence + 1;
                            String sqloc = "update question set occurence = ? where id = '" + qid + "'";
                            try{
                                ps = conn.prepareStatement(sqloc);
                                ps.setInt(1, ocdb);
                                ps.execute();
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            //end for occurence
                            gotQ10++;
                            al10R.add(no);
                        }
                    }
                }
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
    }
    private void getQuestions40Pattern1WithOption(){
        //FOR CHAPTERS Name
        int i;
        String ch1 = "";
        for(i=0; i<lChapter.getSelectedValuesList().size(); i++){
            if(i!=lChapter.getSelectedValuesList().size()-1)
                ch1 += "'" + lChapter.getSelectedValuesList().get(i) + "',";
            else
                ch1 += "'" + lChapter.getSelectedValuesList().get(i) + "'";
        }
        //For chapters id
        String sel = "";
        try{
            Object oarray[] = lChapter.getSelectedValues();
            Vector v = new Vector();
            for (Object oarray1 : oarray) {
                String sqlcid = "select id from chapter where chapter_name = '" + oarray1 + "'";
                ps = conn.prepareStatement(sqlcid);
                rs = ps.executeQuery(); 
                if(rs.next())
                    v.addElement(rs.getInt(1));
            }
            int len = v.toString().length();
            sel = v.toString().substring(1,len-1);
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        //FOR SUBJECTS
        int id = 0;
        try{
            String sname = cmbSubject.getSelectedItem().toString();
            String sqlid = "Select id from subject where subject_name = '" + sname + "'";
            ps = conn.prepareStatement(sqlid);
            rs = ps.executeQuery();
            while(rs.next()){
                int idTemp = rs.getInt("id");
                id = idTemp;
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        
        String selectedDiff = cmbDiff.getSelectedItem().toString();
        String selectedSubject = cmbSubject.getSelectedItem().toString();
        int Marks4,Marks8,Marks10;
        Marks4 = 3 + 1;
        Marks8 = 1 + 1;
        Marks10 = 2 + 1;
        int no;
        int gotQ4 = 0, gotQ8 = 0, gotQ10 = 0;
        
        
        //Output
        System.out.println("Subject Name : " + selectedSubject);
        System.out.println("Chapters : " + ch1);
        System.out.println("Total Marks : 40");
        System.out.println("Duration : 1 Hour 30 Minutes");
        //4 Marks
        try{
            ArrayList<String> al4 = new ArrayList<>();
            ArrayList<String> al4m = new ArrayList<>();
            ArrayList<Integer> al4o = new ArrayList<>();
            String sql = "select question,weightage,occurence from question where weightage = 4 and subject_id = '" + id + "' and difficulty = '" + selectedDiff + "' and chapter_id IN (" + sel + ") ORDER BY occurence LIMIT " + Marks4 + ";";            
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
                String question = rs.getString("question");
                String m = rs.getString("weightage");
                int o = rs.getInt("occurence");
                al4.add(question);
                al4m.add(m);
                al4o.add(o);
//                    System.out.println(al4);
            }
            if(Marks4!=0){
                if(al4.isEmpty()){
                    JOptionPane.showMessageDialog(this, "There are no enough 4 marks question from the selected chapters! Please retry");
                }else{
                    ArrayList<Integer> al4R = new ArrayList<Integer>();
                    System.out.println("Q1. 4 Marks Questions Attempt Any 3");
                    while(Marks4!=gotQ4){
                        no = random.nextInt(al4.size());
                        if(al4R.contains(no)){
                            continue;
                        }else{
                            String quest = al4.get(no);
                            String marks = al4m.get(no);
                            int occurence = al4o.get(no);
                            System.out.println(quest);
                            System.out.println(marks);
                            //start for occurence
                            int qid = 0;
                            String sqlq = "select id from question where question = '" + quest + "'";
                            try{
                                ps = conn.prepareStatement(sqlq);
                                rs = ps.executeQuery();
                                while(rs.next()){
                                    int qidtemp = rs.getInt("id");
                                    qid = qidtemp;
                                }
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            int ocdb = occurence + 1;
                            String sqloc = "update question set occurence = ? where id = '" + qid + "'";
                            try{
                                ps = conn.prepareStatement(sqloc);
                                ps.setInt(1, ocdb);
                                ps.execute();
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            //end for occurence
                            gotQ4++;
                            al4R.add(no);
                        }
                    }
                }
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        //8 Marks
        try{
            ArrayList<String> al8 = new ArrayList<>();
            ArrayList<String> al8m = new ArrayList<>();
            ArrayList<Integer> al8o = new ArrayList<>();
            String sql = "select question,weightage,occurence from question where weightage = 8 and subject_id = '" + id + "' and difficulty = '" + selectedDiff + "' and chapter_id IN (" + sel + ") ORDER BY occurence LIMIT " + Marks8 + ";";            
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
                String question = rs.getString("question");
                String m = rs.getString("weightage");
                int o = rs.getInt("occurence");
                al8.add(question);
                al8m.add(m);
                al8o.add(o);
//                    System.out.println(al8);
            }
            if(Marks8!=0){
                if(al8.isEmpty()){
                    JOptionPane.showMessageDialog(this, "There are no enough 8 marks question from the selected chapters! Please retry");
                }else{
                    ArrayList<Integer> al8R = new ArrayList<Integer>();
                    System.out.println("Q2. 8 Marks Questions Attempt Any 1");
                    while(Marks8!=gotQ8){
                        no = random.nextInt(al8.size());
                        if(al8R.contains(no)){
                            continue;
                        }else{
                            String quest = al8.get(no);
                            String marks = al8m.get(no);
                            int occurence = al8o.get(no);
                            System.out.println(quest);
                            System.out.println(marks);
                            //start for occurence
                            int qid = 0;
                            String sqlq = "select id from question where question = '" + quest + "'";
                            try{
                                ps = conn.prepareStatement(sqlq);
                                rs = ps.executeQuery();
                                while(rs.next()){
                                    int qidtemp = rs.getInt("id");
                                    qid = qidtemp;
                                }
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            int ocdb = occurence + 1;
                            String sqloc = "update question set occurence = ? where id = '" + qid + "'";
                            try{
                                ps = conn.prepareStatement(sqloc);
                                ps.setInt(1, ocdb);
                                ps.execute();
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            //end for occurence
                            gotQ8++;
                            al8R.add(no);
                        }
                    }
                }
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        //10 Marks
        try{
            ArrayList<String> al10 = new ArrayList<>();
            ArrayList<String> al10m = new ArrayList<>();
            ArrayList<Integer> al10o = new ArrayList<>();
            String sql = "select question,weightage,occurence from question where weightage = 10 and subject_id = '" + id + "' and difficulty = '" + selectedDiff + "' and chapter_id IN (" + sel + ") ORDER BY occurence LIMIT " + Marks10 + ";";            
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
                String question = rs.getString("question");
                String m = rs.getString("weightage");
                int o = rs.getInt("occurence");
                al10.add(question);
                al10m.add(m);
                al10o.add(o);
//                    System.out.println(al4);
            }
            if(Marks10!=0){
                if(al10.isEmpty()){
                    JOptionPane.showMessageDialog(this, "There are no enough 10 marks question from the selected chapters! Please retry");
                }else{
                    ArrayList<Integer> al10R = new ArrayList<Integer>();
                    System.out.println("Q3. 10 Marks Questions Attempt Any 2");
                    while(Marks10!=gotQ10){
                        no = random.nextInt(al10.size());
                        if(al10R.contains(no)){
                            continue;
                        }else{
                            String quest = al10.get(no);
                            String marks = al10m.get(no);
                            int occurence = al10o.get(no);
                            System.out.println(quest);
                            System.out.println(marks);
                            //start for occurence
                            int qid = 0;
                            String sqlq = "select id from question where question = '" + quest + "'";
                            try{
                                ps = conn.prepareStatement(sqlq);
                                rs = ps.executeQuery();
                                while(rs.next()){
                                    int qidtemp = rs.getInt("id");
                                    qid = qidtemp;
                                }
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            int ocdb = occurence + 1;
                            String sqloc = "update question set occurence = ? where id = '" + qid + "'";
                            try{
                                ps = conn.prepareStatement(sqloc);
                                ps.setInt(1, ocdb);
                                ps.execute();
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            //end for occurence
                            gotQ10++;
                            al10R.add(no);
                        }
                    }
                }
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
    }
    private void getQuestions40Pattern2WithoutOption(){
        //FOR CHAPTERS Name
        int i;
        String ch1 = "";
        for(i=0; i<lChapter.getSelectedValuesList().size(); i++){
            if(i!=lChapter.getSelectedValuesList().size()-1)
                ch1 += "'" + lChapter.getSelectedValuesList().get(i) + "',";
            else
                ch1 += "'" + lChapter.getSelectedValuesList().get(i) + "'";
        }
        //For chapters id
        String sel = "";
        try{
            Object oarray[] = lChapter.getSelectedValues();
            Vector v = new Vector();
            for (Object oarray1 : oarray) {
                String sqlcid = "select id from chapter where chapter_name = '" + oarray1 + "'";
                ps = conn.prepareStatement(sqlcid);
                rs = ps.executeQuery(); 
                if(rs.next())
                    v.addElement(rs.getInt(1));
            }
            int len = v.toString().length();
            sel = v.toString().substring(1,len-1);
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        //FOR SUBJECTS
        int id = 0;
        try{
            String sname = cmbSubject.getSelectedItem().toString();
            String sqlid = "Select id from subject where subject_name = '" + sname + "'";
            ps = conn.prepareStatement(sqlid);
            rs = ps.executeQuery();
            while(rs.next()){
                int idTemp = rs.getInt("id");
                id = idTemp;
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        
        String selectedDiff = cmbDiff.getSelectedItem().toString();
        String selectedSubject = cmbSubject.getSelectedItem().toString();
        int Marks4,Marks8,Marks10;
        Marks4 = 1;
        Marks8 = 2;
        Marks10 = 2;
        int no;
        int gotQ4 = 0, gotQ8 = 0, gotQ10 = 0;
        
        
        //Output
        System.out.println("Subject Name : " + selectedSubject);
        System.out.println("Chapters : " + ch1);
        System.out.println("Total Marks : 40");
        System.out.println("Duration : 1 Hour 30 Minutes");
        //4 Marks
        try{
            ArrayList<String> al4 = new ArrayList<>();
            ArrayList<String> al4m = new ArrayList<>();
            ArrayList<Integer> al4o = new ArrayList<>();
            String sql = "select question,weightage,occurence from question where weightage = 4 and subject_id = '" + id + "' and difficulty = '" + selectedDiff + "' and chapter_id IN (" + sel + ") ORDER BY occurence LIMIT " + Marks4 + ";";            
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
                String question = rs.getString("question");
                String m = rs.getString("weightage");
                int o = rs.getInt("occurence");
                al4.add(question);
                al4m.add(m);
                al4o.add(o);
//                    System.out.println(al4);
            }
            if(Marks4!=0){
                if(al4.isEmpty()){
                    JOptionPane.showMessageDialog(this, "There are no enough 4 marks question from the selected chapters! Please retry");
                }else{
                    ArrayList<Integer> al4R = new ArrayList<Integer>();
                    System.out.println("Q1. 4 Marks Questions");
                    while(Marks4!=gotQ4){
                        no = random.nextInt(al4.size());
                        if(al4R.contains(no)){
                            continue;
                        }else{
                            String quest = al4.get(no);
                            String marks = al4m.get(no);
                            int occurence = al4o.get(no);
                            System.out.println(quest);
                            System.out.println(marks);
                            //start for occurence
                            int qid = 0;
                            String sqlq = "select id from question where question = '" + quest + "'";
                            try{
                                ps = conn.prepareStatement(sqlq);
                                rs = ps.executeQuery();
                                while(rs.next()){
                                    int qidtemp = rs.getInt("id");
                                    qid = qidtemp;
                                }
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            int ocdb = occurence + 1;
                            String sqloc = "update question set occurence = ? where id = '" + qid + "'";
                            try{
                                ps = conn.prepareStatement(sqloc);
                                ps.setInt(1, ocdb);
                                ps.execute();
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            //end for occurence
                            gotQ4++;
                            al4R.add(no);
                        }
                    }
                }
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        //8 Marks
        try{
            ArrayList<String> al8 = new ArrayList<>();
            ArrayList<String> al8m = new ArrayList<>();
            ArrayList<Integer> al8o = new ArrayList<>();
            String sql = "select question,weightage,occurence from question where weightage = 8 and subject_id = '" + id + "' and difficulty = '" + selectedDiff + "' and chapter_id IN (" + sel + ") ORDER BY occurence LIMIT " + Marks8 + ";";            
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
                String question = rs.getString("question");
                String m = rs.getString("weightage");
                int o = rs.getInt("occurence");
                al8.add(question);
                al8m.add(m);
                al8o.add(o);
//                    System.out.println(al8);
            }
            if(Marks8!=0){
                if(al8.isEmpty()){
                    JOptionPane.showMessageDialog(this, "There are no enough 8 marks question from the selected chapters! Please retry");
                }else{
                    ArrayList<Integer> al8R = new ArrayList<Integer>();
                    System.out.println("Q2. 8 Marks Questions");
                    while(Marks8!=gotQ8){
                        no = random.nextInt(al8.size());
                        if(al8R.contains(no)){
                            continue;
                        }else{
                            String quest = al8.get(no);
                            String marks = al8m.get(no);
                            int occurence = al8o.get(no);
                            System.out.println(quest);
                            System.out.println(marks);
                            //start for occurence
                            int qid = 0;
                            String sqlq = "select id from question where question = '" + quest + "'";
                            try{
                                ps = conn.prepareStatement(sqlq);
                                rs = ps.executeQuery();
                                while(rs.next()){
                                    int qidtemp = rs.getInt("id");
                                    qid = qidtemp;
                                }
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            int ocdb = occurence + 1;
                            String sqloc = "update question set occurence = ? where id = '" + qid + "'";
                            try{
                                ps = conn.prepareStatement(sqloc);
                                ps.setInt(1, ocdb);
                                ps.execute();
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            //end for occurence
                            gotQ8++;
                            al8R.add(no);
                        }
                    }
                }
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        //10 Marks
        try{
            ArrayList<String> al10 = new ArrayList<>();
            ArrayList<String> al10m = new ArrayList<>();
            ArrayList<Integer> al10o = new ArrayList<>();
            String sql = "select question,weightage,occurence from question where weightage = 10 and subject_id = '" + id + "' and difficulty = '" + selectedDiff + "' and chapter_id IN (" + sel + ") ORDER BY occurence LIMIT " + Marks10 + ";";            
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
                String question = rs.getString("question");
                String m = rs.getString("weightage");
                int o = rs.getInt("occurence");
                al10.add(question);
                al10m.add(m);
                al10o.add(o);
//                    System.out.println(al4);
            }
            if(Marks10!=0){
                if(al10.isEmpty()){
                    JOptionPane.showMessageDialog(this, "There are no enough 10 marks question from the selected chapters! Please retry");
                }else{
                    ArrayList<Integer> al10R = new ArrayList<Integer>();
                    System.out.println("Q3. 10 Marks Questions");
                    while(Marks10!=gotQ10){
                        no = random.nextInt(al10.size());
                        if(al10R.contains(no)){
                            continue;
                        }else{
                            String quest = al10.get(no);
                            String marks = al10m.get(no);
                            int occurence = al10o.get(no);
                            System.out.println(quest);
                            System.out.println(marks);
                            //start for occurence
                            int qid = 0;
                            String sqlq = "select id from question where question = '" + quest + "'";
                            try{
                                ps = conn.prepareStatement(sqlq);
                                rs = ps.executeQuery();
                                while(rs.next()){
                                    int qidtemp = rs.getInt("id");
                                    qid = qidtemp;
                                }
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            int ocdb = occurence + 1;
                            String sqloc = "update question set occurence = ? where id = '" + qid + "'";
                            try{
                                ps = conn.prepareStatement(sqloc);
                                ps.setInt(1, ocdb);
                                ps.execute();
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            //end for occurence
                            gotQ10++;
                            al10R.add(no);
                        }
                    }
                }
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
    } 
    private void getQuestions40Pattern2WithOption(){
        //FOR CHAPTERS Name
        int i;
        String ch1 = "";
        for(i=0; i<lChapter.getSelectedValuesList().size(); i++){
            if(i!=lChapter.getSelectedValuesList().size()-1)
                ch1 += "'" + lChapter.getSelectedValuesList().get(i) + "',";
            else
                ch1 += "'" + lChapter.getSelectedValuesList().get(i) + "'";
        }
        //For chapters id
        String sel = "";
        try{
            Object oarray[] = lChapter.getSelectedValues();
            Vector v = new Vector();
            for (Object oarray1 : oarray) {
                String sqlcid = "select id from chapter where chapter_name = '" + oarray1 + "'";
                ps = conn.prepareStatement(sqlcid);
                rs = ps.executeQuery(); 
                if(rs.next())
                    v.addElement(rs.getInt(1));
            }
            int len = v.toString().length();
            sel = v.toString().substring(1,len-1);
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        //FOR SUBJECTS
        int id = 0;
        try{
            String sname = cmbSubject.getSelectedItem().toString();
            String sqlid = "Select id from subject where subject_name = '" + sname + "'";
            ps = conn.prepareStatement(sqlid);
            rs = ps.executeQuery();
            while(rs.next()){
                int idTemp = rs.getInt("id");
                id = idTemp;
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        
        String selectedDiff = cmbDiff.getSelectedItem().toString();
        String selectedSubject = cmbSubject.getSelectedItem().toString();
        int Marks4,Marks8,Marks10;
        Marks4 = 1 + 1;
        Marks8 = 2 + 1;
        Marks10 = 2 + 1;
        int no;
        int gotQ4 = 0, gotQ8 = 0, gotQ10 = 0;
        
        
        //Output
        System.out.println("Subject Name : " + selectedSubject);
        System.out.println("Chapters : " + ch1);
        System.out.println("Total Marks : 40");
        System.out.println("Duration : 1 Hour 30 Minutes");
        //4 Marks
        try{
            ArrayList<String> al4 = new ArrayList<>();
            ArrayList<String> al4m = new ArrayList<>();
            ArrayList<Integer> al4o = new ArrayList<>();
            String sql = "select question,weightage,occurence from question where weightage = 4 and subject_id = '" + id + "' and difficulty = '" + selectedDiff + "' and chapter_id IN (" + sel + ") ORDER BY occurence LIMIT " + Marks4 + ";";            
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
                String question = rs.getString("question");
                String m = rs.getString("weightage");
                int o = rs.getInt("occurence");
                al4.add(question);
                al4m.add(m);
                al4o.add(o);
//                    System.out.println(al4);
            }
            if(Marks4!=0){
                if(al4.isEmpty()){
                    JOptionPane.showMessageDialog(this, "There are no enough 4 marks question from the selected chapters! Please retry");
                }else{
                    ArrayList<Integer> al4R = new ArrayList<Integer>();
                    System.out.println("Q1. 4 Marks Questions Attempt Any 1");
                    while(Marks4!=gotQ4){
                        no = random.nextInt(al4.size());
                        if(al4R.contains(no)){
                            continue;
                        }else{
                            String quest = al4.get(no);
                            String marks = al4m.get(no);
                            int occurence = al4o.get(no);
                            System.out.println(quest);
                            System.out.println(marks);
                            //start for occurence
                            int qid = 0;
                            String sqlq = "select id from question where question = '" + quest + "'";
                            try{
                                ps = conn.prepareStatement(sqlq);
                                rs = ps.executeQuery();
                                while(rs.next()){
                                    int qidtemp = rs.getInt("id");
                                    qid = qidtemp;
                                }
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            int ocdb = occurence + 1;
                            String sqloc = "update question set occurence = ? where id = '" + qid + "'";
                            try{
                                ps = conn.prepareStatement(sqloc);
                                ps.setInt(1, ocdb);
                                ps.execute();
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            //end for occurence
                            gotQ4++;
                            al4R.add(no);
                        }
                    }
                }
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        //8 Marks
        try{
            ArrayList<String> al8 = new ArrayList<>();
            ArrayList<String> al8m = new ArrayList<>();
            ArrayList<Integer> al8o = new ArrayList<>();
            String sql = "select question,weightage,occurence from question where weightage = 8 and subject_id = '" + id + "' and difficulty = '" + selectedDiff + "' and chapter_id IN (" + sel + ") ORDER BY occurence LIMIT " + Marks8 + ";";            
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
                String question = rs.getString("question");
                String m = rs.getString("weightage");
                int o = rs.getInt("occurence");
                al8.add(question);
                al8m.add(m);
                al8o.add(o);
//                    System.out.println(al8);
            }
            if(Marks8!=0){
                if(al8.isEmpty()){
                    JOptionPane.showMessageDialog(this, "There are no enough 8 marks question from the selected chapters! Please retry");
                }else{
                    ArrayList<Integer> al8R = new ArrayList<Integer>();
                    System.out.println("Q2. 8 Marks Questions Attempt Any 2");
                    while(Marks8!=gotQ8){
                        no = random.nextInt(al8.size());
                        if(al8R.contains(no)){
                            continue;
                        }else{
                            String quest = al8.get(no);
                            String marks = al8m.get(no);
                            int occurence = al8o.get(no);
                            System.out.println(quest);
                            System.out.println(marks);
                            //start for occurence
                            int qid = 0;
                            String sqlq = "select id from question where question = '" + quest + "'";
                            try{
                                ps = conn.prepareStatement(sqlq);
                                rs = ps.executeQuery();
                                while(rs.next()){
                                    int qidtemp = rs.getInt("id");
                                    qid = qidtemp;
                                }
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            int ocdb = occurence + 1;
                            String sqloc = "update question set occurence = ? where id = '" + qid + "'";
                            try{
                                ps = conn.prepareStatement(sqloc);
                                ps.setInt(1, ocdb);
                                ps.execute();
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            //end for occurence
                            gotQ8++;
                            al8R.add(no);
                        }
                    }
                }
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        //10 Marks
        try{
            ArrayList<String> al10 = new ArrayList<>();
            ArrayList<String> al10m = new ArrayList<>();
            ArrayList<Integer> al10o = new ArrayList<>();
            String sql = "select question,weightage,occurence from question where weightage = 10 and subject_id = '" + id + "' and difficulty = '" + selectedDiff + "' and chapter_id IN (" + sel + ") ORDER BY occurence LIMIT " + Marks10 + ";";            
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
                String question = rs.getString("question");
                String m = rs.getString("weightage");
                int o = rs.getInt("occurence");
                al10.add(question);
                al10m.add(m);
                al10o.add(o);
//                    System.out.println(al4);
            }
            if(Marks10!=0){
                if(al10.isEmpty()){
                    JOptionPane.showMessageDialog(this, "There are no enough 10 marks question from the selected chapters! Please retry");
                }else{
                    ArrayList<Integer> al10R = new ArrayList<Integer>();
                    System.out.println("Q3. 10 Marks Questions Attempt Any 2");
                    while(Marks10!=gotQ10){
                        no = random.nextInt(al10.size());
                        if(al10R.contains(no)){
                            continue;
                        }else{
                            String quest = al10.get(no);
                            String marks = al10m.get(no);
                            int occurence = al10o.get(no);
                            System.out.println(quest);
                            System.out.println(marks);
                            //start for occurence
                            int qid = 0;
                            String sqlq = "select id from question where question = '" + quest + "'";
                            try{
                                ps = conn.prepareStatement(sqlq);
                                rs = ps.executeQuery();
                                while(rs.next()){
                                    int qidtemp = rs.getInt("id");
                                    qid = qidtemp;
                                }
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            int ocdb = occurence + 1;
                            String sqloc = "update question set occurence = ? where id = '" + qid + "'";
                            try{
                                ps = conn.prepareStatement(sqloc);
                                ps.setInt(1, ocdb);
                                ps.execute();
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            //end for occurence
                            gotQ10++;
                            al10R.add(no);
                        }
                    }
                }
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
    }
    //80
    private void getQuestions80Pattern1WithoutOption(){
        //FOR CHAPTERS Name
        int i;
        String ch1 = "";
        for(i=0; i<lChapter.getSelectedValuesList().size(); i++){
            if(i!=lChapter.getSelectedValuesList().size()-1)
                ch1 += "'" + lChapter.getSelectedValuesList().get(i) + "',";
            else
                ch1 += "'" + lChapter.getSelectedValuesList().get(i) + "'";
        }
        //For chapters id
        String sel = "";
        try{
            Object oarray[] = lChapter.getSelectedValues();
            Vector v = new Vector();
            for (Object oarray1 : oarray) {
                String sqlcid = "select id from chapter where chapter_name = '" + oarray1 + "'";
                ps = conn.prepareStatement(sqlcid);
                rs = ps.executeQuery(); 
                if(rs.next())
                    v.addElement(rs.getInt(1));
            }
            int len = v.toString().length();
            sel = v.toString().substring(1,len-1);
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        //FOR SUBJECTS
        int id = 0;
        try{
            String sname = cmbSubject.getSelectedItem().toString();
            String sqlid = "Select id from subject where subject_name = '" + sname + "'";
            ps = conn.prepareStatement(sqlid);
            rs = ps.executeQuery();
            while(rs.next()){
                int idTemp = rs.getInt("id");
                id = idTemp;
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        
        String selectedDiff = cmbDiff.getSelectedItem().toString();
        String selectedSubject = cmbSubject.getSelectedItem().toString();
        int Marks4,Marks8,Marks10;
        Marks4 = 6;
        Marks8 = 2;
        Marks10 = 4;
        int no;
        int gotQ4 = 0, gotQ8 = 0, gotQ10 = 0;
        
        
        //Output
        System.out.println("Subject Name : " + selectedSubject);
        System.out.println("Chapters : " + ch1);
        System.out.println("Total Marks : 80");
        System.out.println("Duration : 3 Hours");
        //4 Marks
        try{
            ArrayList<String> al4 = new ArrayList<>();
            ArrayList<String> al4m = new ArrayList<>();
            ArrayList<Integer> al4o = new ArrayList<>();
            String sql = "select question,weightage,occurence from question where weightage = 4 and subject_id = '" + id + "' and difficulty = '" + selectedDiff + "' and chapter_id IN (" + sel + ") ORDER BY occurence LIMIT " + Marks4 + ";";            
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
                String question = rs.getString("question");
                String m = rs.getString("weightage");
                int o = rs.getInt("occurence");
                al4.add(question);
                al4m.add(m);
                al4o.add(o);
//                    System.out.println(al4);
            }
            if(Marks4!=0){
                if(al4.isEmpty()){
                    JOptionPane.showMessageDialog(this, "There are no enough 4 marks question from the selected chapters! Please retry");
                }else{
                    ArrayList<Integer> al4R = new ArrayList<Integer>();
                    System.out.println("Q1. 4 Marks Questions");
                    while(Marks4!=gotQ4){
                        no = random.nextInt(al4.size());
                        if(al4R.contains(no)){
                            continue;
                        }else{
                            String quest = al4.get(no);
                            String marks = al4m.get(no);
                            int occurence = al4o.get(no);
                            System.out.println(quest);
                            System.out.println(marks);
                            //start for occurence
                            int qid = 0;
                            String sqlq = "select id from question where question = '" + quest + "'";
                            try{
                                ps = conn.prepareStatement(sqlq);
                                rs = ps.executeQuery();
                                while(rs.next()){
                                    int qidtemp = rs.getInt("id");
                                    qid = qidtemp;
                                }
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            int ocdb = occurence + 1;
                            String sqloc = "update question set occurence = ? where id = '" + qid + "'";
                            try{
                                ps = conn.prepareStatement(sqloc);
                                ps.setInt(1, ocdb);
                                ps.execute();
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            //end for occurence
                            gotQ4++;
                            al4R.add(no);
                        }
                    }
                }
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        //8 Marks
        try{
            ArrayList<String> al8 = new ArrayList<>();
            ArrayList<String> al8m = new ArrayList<>();
            ArrayList<Integer> al8o = new ArrayList<>();
            String sql = "select question,weightage,occurence from question where weightage = 8 and subject_id = '" + id + "' and difficulty = '" + selectedDiff + "' and chapter_id IN (" + sel + ") ORDER BY occurence LIMIT " + Marks8 + ";";            
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
                String question = rs.getString("question");
                String m = rs.getString("weightage");
                int o = rs.getInt("occurence");
                al8.add(question);
                al8m.add(m);
                al8o.add(o);
//                    System.out.println(al8);
            }
            if(Marks8!=0){
                if(al8.isEmpty()){
                    JOptionPane.showMessageDialog(this, "There are no enough 8 marks question from the selected chapters! Please retry");
                }else{
                    ArrayList<Integer> al8R = new ArrayList<Integer>();
                    System.out.println("Q2. 8 Marks Questions");
                    while(Marks8!=gotQ8){
                        no = random.nextInt(al8.size());
                        if(al8R.contains(no)){
                            continue;
                        }else{
                            String quest = al8.get(no);
                            String marks = al8m.get(no);
                            int occurence = al8o.get(no);
                            System.out.println(quest);
                            System.out.println(marks);
                            //start for occurence
                            int qid = 0;
                            String sqlq = "select id from question where question = '" + quest + "'";
                            try{
                                ps = conn.prepareStatement(sqlq);
                                rs = ps.executeQuery();
                                while(rs.next()){
                                    int qidtemp = rs.getInt("id");
                                    qid = qidtemp;
                                }
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            int ocdb = occurence + 1;
                            String sqloc = "update question set occurence = ? where id = '" + qid + "'";
                            try{
                                ps = conn.prepareStatement(sqloc);
                                ps.setInt(1, ocdb);
                                ps.execute();
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            //end for occurence
                            gotQ8++;
                            al8R.add(no);
                        }
                    }
                }
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        //10 Marks
        try{
            ArrayList<String> al10 = new ArrayList<>();
            ArrayList<String> al10m = new ArrayList<>();
            ArrayList<Integer> al10o = new ArrayList<>();
            String sql = "select question,weightage,occurence from question where weightage = 10 and subject_id = '" + id + "' and difficulty = '" + selectedDiff + "' and chapter_id IN (" + sel + ") ORDER BY occurence LIMIT " + Marks10 + ";";            
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
                String question = rs.getString("question");
                String m = rs.getString("weightage");
                int o = rs.getInt("occurence");
                al10.add(question);
                al10m.add(m);
                al10o.add(o);
//                    System.out.println(al4);
            }
            if(Marks10!=0){
                if(al10.isEmpty()){
                    JOptionPane.showMessageDialog(this, "There are no enough 10 marks question from the selected chapters! Please retry");
                }else{
                    ArrayList<Integer> al10R = new ArrayList<Integer>();
                    System.out.println("Q3. 10 Marks Questions");
                    while(Marks10!=gotQ10){
                        no = random.nextInt(al10.size());
                        if(al10R.contains(no)){
                            continue;
                        }else{
                            String quest = al10.get(no);
                            String marks = al10m.get(no);
                            int occurence = al10o.get(no);
                            System.out.println(quest);
                            System.out.println(marks);
                            //start for occurence
                            int qid = 0;
                            String sqlq = "select id from question where question = '" + quest + "'";
                            try{
                                ps = conn.prepareStatement(sqlq);
                                rs = ps.executeQuery();
                                while(rs.next()){
                                    int qidtemp = rs.getInt("id");
                                    qid = qidtemp;
                                }
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            int ocdb = occurence + 1;
                            String sqloc = "update question set occurence = ? where id = '" + qid + "'";
                            try{
                                ps = conn.prepareStatement(sqloc);
                                ps.setInt(1, ocdb);
                                ps.execute();
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            //end for occurence
                            gotQ10++;
                            al10R.add(no);
                        }
                    }
                }
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
    }
    private void getQuestions80Pattern1WithOption(){
        //FOR CHAPTERS Name
        int i;
        String ch1 = "";
        for(i=0; i<lChapter.getSelectedValuesList().size(); i++){
            if(i!=lChapter.getSelectedValuesList().size()-1)
                ch1 += "'" + lChapter.getSelectedValuesList().get(i) + "',";
            else
                ch1 += "'" + lChapter.getSelectedValuesList().get(i) + "'";
        }
        //For chapters id
        String sel = "";
        try{
            Object oarray[] = lChapter.getSelectedValues();
            Vector v = new Vector();
            for (Object oarray1 : oarray) {
                String sqlcid = "select id from chapter where chapter_name = '" + oarray1 + "'";
                ps = conn.prepareStatement(sqlcid);
                rs = ps.executeQuery(); 
                if(rs.next())
                    v.addElement(rs.getInt(1));
            }
            int len = v.toString().length();
            sel = v.toString().substring(1,len-1);
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        //FOR SUBJECTS
        int id = 0;
        try{
            String sname = cmbSubject.getSelectedItem().toString();
            String sqlid = "Select id from subject where subject_name = '" + sname + "'";
            ps = conn.prepareStatement(sqlid);
            rs = ps.executeQuery();
            while(rs.next()){
                int idTemp = rs.getInt("id");
                id = idTemp;
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        
        String selectedDiff = cmbDiff.getSelectedItem().toString();
        String selectedSubject = cmbSubject.getSelectedItem().toString();
        int Marks4,Marks8,Marks10;
        Marks4 = 6 + 2;
        Marks8 = 2 + 2;
        Marks10 = 4 + 2;
        int no;
        int gotQ4 = 0, gotQ8 = 0, gotQ10 = 0;
        
        
        //Output
        System.out.println("Subject Name : " + selectedSubject);
        System.out.println("Chapters : " + ch1);
        System.out.println("Total Marks : 80");
        System.out.println("Duration : 3 Hours");
        //4 Marks
        try{
            ArrayList<String> al4 = new ArrayList<>();
            ArrayList<String> al4m = new ArrayList<>();
            ArrayList<Integer> al4o = new ArrayList<>();
            String sql = "select question,weightage,occurence from question where weightage = 4 and subject_id = '" + id + "' and difficulty = '" + selectedDiff + "' and chapter_id IN (" + sel + ") ORDER BY occurence LIMIT " + Marks4 + ";";            
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
                String question = rs.getString("question");
                String m = rs.getString("weightage");
                int o = rs.getInt("occurence");
                al4.add(question);
                al4m.add(m);
                al4o.add(o);
//                    System.out.println(al4);
            }
            if(Marks4!=0){
                if(al4.isEmpty()){
                    JOptionPane.showMessageDialog(this, "There are no enough 4 marks question from the selected chapters! Please retry");
                }else{
                    ArrayList<Integer> al4R = new ArrayList<Integer>();
                    System.out.println("Q1. 4 Marks Questions Attempt Any 6");
                    while(Marks4!=gotQ4){
                        no = random.nextInt(al4.size());
                        if(al4R.contains(no)){
                            continue;
                        }else{
                            String quest = al4.get(no);
                            String marks = al4m.get(no);
                            int occurence = al4o.get(no);
                            System.out.println(quest);
                            System.out.println(marks);
                            //start for occurence
                            int qid = 0;
                            String sqlq = "select id from question where question = '" + quest + "'";
                            try{
                                ps = conn.prepareStatement(sqlq);
                                rs = ps.executeQuery();
                                while(rs.next()){
                                    int qidtemp = rs.getInt("id");
                                    qid = qidtemp;
                                }
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            int ocdb = occurence + 1;
                            String sqloc = "update question set occurence = ? where id = '" + qid + "'";
                            try{
                                ps = conn.prepareStatement(sqloc);
                                ps.setInt(1, ocdb);
                                ps.execute();
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            //end for occurence
                            gotQ4++;
                            al4R.add(no);
                        }
                    }
                }
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        //8 Marks
        try{
            ArrayList<String> al8 = new ArrayList<>();
            ArrayList<String> al8m = new ArrayList<>();
            ArrayList<Integer> al8o = new ArrayList<>();
            String sql = "select question,weightage,occurence from question where weightage = 8 and subject_id = '" + id + "' and difficulty = '" + selectedDiff + "' and chapter_id IN (" + sel + ") ORDER BY occurence LIMIT " + Marks8 + ";";            
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
                String question = rs.getString("question");
                String m = rs.getString("weightage");
                int o = rs.getInt("occurence");
                al8.add(question);
                al8m.add(m);
                al8o.add(o);
//                    System.out.println(al8);
            }
            if(Marks8!=0){
                if(al8.isEmpty()){
                    JOptionPane.showMessageDialog(this, "There are no enough 8 marks question from the selected chapters! Please retry");
                }else{
                    ArrayList<Integer> al8R = new ArrayList<Integer>();
                    System.out.println("Q2. 8 Marks Questions Attempt Any 2");
                    while(Marks8!=gotQ8){
                        no = random.nextInt(al8.size());
                        if(al8R.contains(no)){
                            continue;
                        }else{
                            String quest = al8.get(no);
                            String marks = al8m.get(no);
                            int occurence = al8o.get(no);
                            System.out.println(quest);
                            System.out.println(marks);
                            //start for occurence
                            int qid = 0;
                            String sqlq = "select id from question where question = '" + quest + "'";
                            try{
                                ps = conn.prepareStatement(sqlq);
                                rs = ps.executeQuery();
                                while(rs.next()){
                                    int qidtemp = rs.getInt("id");
                                    qid = qidtemp;
                                }
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            int ocdb = occurence + 1;
                            String sqloc = "update question set occurence = ? where id = '" + qid + "'";
                            try{
                                ps = conn.prepareStatement(sqloc);
                                ps.setInt(1, ocdb);
                                ps.execute();
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            //end for occurence
                            gotQ8++;
                            al8R.add(no);
                        }
                    }
                }
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        //10 Marks
        try{
            ArrayList<String> al10 = new ArrayList<>();
            ArrayList<String> al10m = new ArrayList<>();
            ArrayList<Integer> al10o = new ArrayList<>();
            String sql = "select question,weightage,occurence from question where weightage = 10 and subject_id = '" + id + "' and difficulty = '" + selectedDiff + "' and chapter_id IN (" + sel + ") ORDER BY occurence LIMIT " + Marks10 + ";";            
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
                String question = rs.getString("question");
                String m = rs.getString("weightage");
                int o = rs.getInt("occurence");
                al10.add(question);
                al10m.add(m);
                al10o.add(o);
//                    System.out.println(al4);
            }
            if(Marks10!=0){
                if(al10.isEmpty()){
                    JOptionPane.showMessageDialog(this, "There are no enough 10 marks question from the selected chapters! Please retry");
                }else{
                    ArrayList<Integer> al10R = new ArrayList<Integer>();
                    System.out.println("Q3. 10 Marks Questions Attempt Any 4");
                    while(Marks10!=gotQ10){
                        no = random.nextInt(al10.size());
                        if(al10R.contains(no)){
                            continue;
                        }else{
                            String quest = al10.get(no);
                            String marks = al10m.get(no);
                            int occurence = al10o.get(no);
                            System.out.println(quest);
                            System.out.println(marks);
                            //start for occurence
                            int qid = 0;
                            String sqlq = "select id from question where question = '" + quest + "'";
                            try{
                                ps = conn.prepareStatement(sqlq);
                                rs = ps.executeQuery();
                                while(rs.next()){
                                    int qidtemp = rs.getInt("id");
                                    qid = qidtemp;
                                }
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            int ocdb = occurence + 1;
                            String sqloc = "update question set occurence = ? where id = '" + qid + "'";
                            try{
                                ps = conn.prepareStatement(sqloc);
                                ps.setInt(1, ocdb);
                                ps.execute();
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            //end for occurence
                            gotQ10++;
                            al10R.add(no);
                        }
                    }
                }
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
    }
    private void getQuestions80Pattern2WithoutOption(){
        //FOR CHAPTERS Name
        int i;
        String ch1 = "";
        for(i=0; i<lChapter.getSelectedValuesList().size(); i++){
            if(i!=lChapter.getSelectedValuesList().size()-1)
                ch1 += "'" + lChapter.getSelectedValuesList().get(i) + "',";
            else
                ch1 += "'" + lChapter.getSelectedValuesList().get(i) + "'";
        }
        //For chapters id
        String sel = "";
        try{
            Object oarray[] = lChapter.getSelectedValues();
            Vector v = new Vector();
            for (Object oarray1 : oarray) {
                String sqlcid = "select id from chapter where chapter_name = '" + oarray1 + "'";
                ps = conn.prepareStatement(sqlcid);
                rs = ps.executeQuery(); 
                if(rs.next())
                    v.addElement(rs.getInt(1));
            }
            int len = v.toString().length();
            sel = v.toString().substring(1,len-1);
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        //FOR SUBJECTS
        int id = 0;
        try{
            String sname = cmbSubject.getSelectedItem().toString();
            String sqlid = "Select id from subject where subject_name = '" + sname + "'";
            ps = conn.prepareStatement(sqlid);
            rs = ps.executeQuery();
            while(rs.next()){
                int idTemp = rs.getInt("id");
                id = idTemp;
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        
        String selectedDiff = cmbDiff.getSelectedItem().toString();
        String selectedSubject = cmbSubject.getSelectedItem().toString();
        int Marks4,Marks8,Marks10;
        Marks4 = 2;
        Marks8 = 4;
        Marks10 = 4;
        int no;
        int gotQ4 = 0, gotQ8 = 0, gotQ10 = 0;
        
        
        //Output
        System.out.println("Subject Name : " + selectedSubject);
        System.out.println("Chapters : " + ch1);
        System.out.println("Total Marks : 80");
        System.out.println("Duration : 3 Hours");
        //4 Marks
        try{
            ArrayList<String> al4 = new ArrayList<>();
            ArrayList<String> al4m = new ArrayList<>();
            ArrayList<Integer> al4o = new ArrayList<>();
            String sql = "select question,weightage,occurence from question where weightage = 4 and subject_id = '" + id + "' and difficulty = '" + selectedDiff + "' and chapter_id IN (" + sel + ") ORDER BY occurence LIMIT " + Marks4 + ";";            
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
                String question = rs.getString("question");
                String m = rs.getString("weightage");
                int o = rs.getInt("occurence");
                al4.add(question);
                al4m.add(m);
                al4o.add(o);
//                    System.out.println(al4);
            }
            if(Marks4!=0){
                if(al4.isEmpty()){
                    JOptionPane.showMessageDialog(this, "There are no enough 4 marks question from the selected chapters! Please retry");
                }else{
                    ArrayList<Integer> al4R = new ArrayList<Integer>();
                    System.out.println("Q1. 4 Marks Questions");
                    while(Marks4!=gotQ4){
                        no = random.nextInt(al4.size());
                        if(al4R.contains(no)){
                            continue;
                        }else{
                            String quest = al4.get(no);
                            String marks = al4m.get(no);
                            int occurence = al4o.get(no);
                            System.out.println(quest);
                            System.out.println(marks);
                            //start for occurence
                            int qid = 0;
                            String sqlq = "select id from question where question = '" + quest + "'";
                            try{
                                ps = conn.prepareStatement(sqlq);
                                rs = ps.executeQuery();
                                while(rs.next()){
                                    int qidtemp = rs.getInt("id");
                                    qid = qidtemp;
                                }
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            int ocdb = occurence + 1;
                            String sqloc = "update question set occurence = ? where id = '" + qid + "'";
                            try{
                                ps = conn.prepareStatement(sqloc);
                                ps.setInt(1, ocdb);
                                ps.execute();
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            //end for occurence
                            gotQ4++;
                            al4R.add(no);
                        }
                    }
                }
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        //8 Marks
        try{
            ArrayList<String> al8 = new ArrayList<>();
            ArrayList<String> al8m = new ArrayList<>();
            ArrayList<Integer> al8o = new ArrayList<>();
            String sql = "select question,weightage,occurence from question where weightage = 8 and subject_id = '" + id + "' and difficulty = '" + selectedDiff + "' and chapter_id IN (" + sel + ") ORDER BY occurence LIMIT " + Marks8 + ";";            
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
                String question = rs.getString("question");
                String m = rs.getString("weightage");
                int o = rs.getInt("occurence");
                al8.add(question);
                al8m.add(m);
                al8o.add(o);
//                    System.out.println(al8);
            }
            if(Marks8!=0){
                if(al8.isEmpty()){
                    JOptionPane.showMessageDialog(this, "There are no enough 8 marks question from the selected chapters! Please retry");
                }else{
                    ArrayList<Integer> al8R = new ArrayList<Integer>();
                    System.out.println("Q2. 8 Marks Questions");
                    while(Marks8!=gotQ8){
                        no = random.nextInt(al8.size());
                        if(al8R.contains(no)){
                            continue;
                        }else{
                            String quest = al8.get(no);
                            String marks = al8m.get(no);
                            int occurence = al8o.get(no);
                            System.out.println(quest);
                            System.out.println(marks);
                            //start for occurence
                            int qid = 0;
                            String sqlq = "select id from question where question = '" + quest + "'";
                            try{
                                ps = conn.prepareStatement(sqlq);
                                rs = ps.executeQuery();
                                while(rs.next()){
                                    int qidtemp = rs.getInt("id");
                                    qid = qidtemp;
                                }
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            int ocdb = occurence + 1;
                            String sqloc = "update question set occurence = ? where id = '" + qid + "'";
                            try{
                                ps = conn.prepareStatement(sqloc);
                                ps.setInt(1, ocdb);
                                ps.execute();
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            //end for occurence
                            gotQ8++;
                            al8R.add(no);
                        }
                    }
                }
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        //10 Marks
        try{
            ArrayList<String> al10 = new ArrayList<>();
            ArrayList<String> al10m = new ArrayList<>();
            ArrayList<Integer> al10o = new ArrayList<>();
            String sql = "select question,weightage,occurence from question where weightage = 10 and subject_id = '" + id + "' and difficulty = '" + selectedDiff + "' and chapter_id IN (" + sel + ") ORDER BY occurence LIMIT " + Marks10 + ";";            
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
                String question = rs.getString("question");
                String m = rs.getString("weightage");
                int o = rs.getInt("occurence");
                al10.add(question);
                al10m.add(m);
                al10o.add(o);
//                    System.out.println(al4);
            }
            if(Marks10!=0){
                if(al10.isEmpty()){
                    JOptionPane.showMessageDialog(this, "There are no enough 10 marks question from the selected chapters! Please retry");
                }else{
                    ArrayList<Integer> al10R = new ArrayList<Integer>();
                    System.out.println("Q3. 10 Marks Questions");
                    while(Marks10!=gotQ10){
                        no = random.nextInt(al10.size());
                        if(al10R.contains(no)){
                            continue;
                        }else{
                            String quest = al10.get(no);
                            String marks = al10m.get(no);
                            int occurence = al10o.get(no);
                            System.out.println(quest);
                            System.out.println(marks);
                            //start for occurence
                            int qid = 0;
                            String sqlq = "select id from question where question = '" + quest + "'";
                            try{
                                ps = conn.prepareStatement(sqlq);
                                rs = ps.executeQuery();
                                while(rs.next()){
                                    int qidtemp = rs.getInt("id");
                                    qid = qidtemp;
                                }
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            int ocdb = occurence + 1;
                            String sqloc = "update question set occurence = ? where id = '" + qid + "'";
                            try{
                                ps = conn.prepareStatement(sqloc);
                                ps.setInt(1, ocdb);
                                ps.execute();
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            //end for occurence
                            gotQ10++;
                            al10R.add(no);
                        }
                    }
                }
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
    } 
    private void getQuestions80Pattern2WithOption(){
        //FOR CHAPTERS Name
        int i;
        String ch1 = "";
        for(i=0; i<lChapter.getSelectedValuesList().size(); i++){
            if(i!=lChapter.getSelectedValuesList().size()-1)
                ch1 += "'" + lChapter.getSelectedValuesList().get(i) + "',";
            else
                ch1 += "'" + lChapter.getSelectedValuesList().get(i) + "'";
        }
        //For chapters id
        String sel = "";
        try{
            Object oarray[] = lChapter.getSelectedValues();
            Vector v = new Vector();
            for (Object oarray1 : oarray) {
                String sqlcid = "select id from chapter where chapter_name = '" + oarray1 + "'";
                ps = conn.prepareStatement(sqlcid);
                rs = ps.executeQuery(); 
                if(rs.next())
                    v.addElement(rs.getInt(1));
            }
            int len = v.toString().length();
            sel = v.toString().substring(1,len-1);
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        //FOR SUBJECTS
        int id = 0;
        try{
            String sname = cmbSubject.getSelectedItem().toString();
            String sqlid = "Select id from subject where subject_name = '" + sname + "'";
            ps = conn.prepareStatement(sqlid);
            rs = ps.executeQuery();
            while(rs.next()){
                int idTemp = rs.getInt("id");
                id = idTemp;
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        
        String selectedDiff = cmbDiff.getSelectedItem().toString();
        String selectedSubject = cmbSubject.getSelectedItem().toString();
        int Marks4,Marks8,Marks10;
        Marks4 = 2 + 1;
        Marks8 = 4 + 2;
        Marks10 = 4 + 2;
        int no;
        int gotQ4 = 0, gotQ8 = 0, gotQ10 = 0;
        
        
        //Output
        System.out.println("Subject Name : " + selectedSubject);
        System.out.println("Chapters : " + ch1);
        System.out.println("Total Marks : 80");
        System.out.println("Duration : 3 Hours");
        //4 Marks
        try{
            ArrayList<String> al4 = new ArrayList<>();
            ArrayList<String> al4m = new ArrayList<>();
            ArrayList<Integer> al4o = new ArrayList<>();
            String sql = "select question,weightage,occurence from question where weightage = 4 and subject_id = '" + id + "' and difficulty = '" + selectedDiff + "' and chapter_id IN (" + sel + ") ORDER BY occurence LIMIT " + Marks4 + ";";            
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
                String question = rs.getString("question");
                String m = rs.getString("weightage");
                int o = rs.getInt("occurence");
                al4.add(question);
                al4m.add(m);
                al4o.add(o);
//                    System.out.println(al4);
            }
            if(Marks4!=0){
                if(al4.isEmpty()){
                    JOptionPane.showMessageDialog(this, "There are no enough 4 marks question from the selected chapters! Please retry");
                }else{
                    ArrayList<Integer> al4R = new ArrayList<Integer>();
                    System.out.println("Q1. 4 Marks Questions Attempt Any 2");
                    while(Marks4!=gotQ4){
                        no = random.nextInt(al4.size());
                        if(al4R.contains(no)){
                            continue;
                        }else{
                            String quest = al4.get(no);
                            String marks = al4m.get(no);
                            int occurence = al4o.get(no);
                            System.out.println(quest);
                            System.out.println(marks);
                            //start for occurence
                            int qid = 0;
                            String sqlq = "select id from question where question = '" + quest + "'";
                            try{
                                ps = conn.prepareStatement(sqlq);
                                rs = ps.executeQuery();
                                while(rs.next()){
                                    int qidtemp = rs.getInt("id");
                                    qid = qidtemp;
                                }
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            int ocdb = occurence + 1;
                            String sqloc = "update question set occurence = ? where id = '" + qid + "'";
                            try{
                                ps = conn.prepareStatement(sqloc);
                                ps.setInt(1, ocdb);
                                ps.execute();
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            //end for occurence
                            gotQ4++;
                            al4R.add(no);
                        }
                    }
                }
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        //8 Marks
        try{
            ArrayList<String> al8 = new ArrayList<>();
            ArrayList<String> al8m = new ArrayList<>();
            ArrayList<Integer> al8o = new ArrayList<>();
            String sql = "select question,weightage,occurence from question where weightage = 8 and subject_id = '" + id + "' and difficulty = '" + selectedDiff + "' and chapter_id IN (" + sel + ") ORDER BY occurence LIMIT " + Marks8 + ";";            
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
                String question = rs.getString("question");
                String m = rs.getString("weightage");
                int o = rs.getInt("occurence");
                al8.add(question);
                al8m.add(m);
                al8o.add(o);
//                    System.out.println(al8);
            }
            if(Marks8!=0){
                if(al8.isEmpty()){
                    JOptionPane.showMessageDialog(this, "There are no enough 8 marks question from the selected chapters! Please retry");
                }else{
                    ArrayList<Integer> al8R = new ArrayList<Integer>();
                    System.out.println("Q2. 8 Marks Questions Attempt Any 4");
                    while(Marks8!=gotQ8){
                        no = random.nextInt(al8.size());
                        if(al8R.contains(no)){
                            continue;
                        }else{
                            String quest = al8.get(no);
                            String marks = al8m.get(no);
                            int occurence = al8o.get(no);
                            System.out.println(quest);
                            System.out.println(marks);
                            //start for occurence
                            int qid = 0;
                            String sqlq = "select id from question where question = '" + quest + "'";
                            try{
                                ps = conn.prepareStatement(sqlq);
                                rs = ps.executeQuery();
                                while(rs.next()){
                                    int qidtemp = rs.getInt("id");
                                    qid = qidtemp;
                                }
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            int ocdb = occurence + 1;
                            String sqloc = "update question set occurence = ? where id = '" + qid + "'";
                            try{
                                ps = conn.prepareStatement(sqloc);
                                ps.setInt(1, ocdb);
                                ps.execute();
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            //end for occurence
                            gotQ8++;
                            al8R.add(no);
                        }
                    }
                }
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        //10 Marks
        try{
            ArrayList<String> al10 = new ArrayList<>();
            ArrayList<String> al10m = new ArrayList<>();
            ArrayList<Integer> al10o = new ArrayList<>();
            String sql = "select question,weightage,occurence from question where weightage = 10 and subject_id = '" + id + "' and difficulty = '" + selectedDiff + "' and chapter_id IN (" + sel + ") ORDER BY occurence LIMIT " + Marks10 + ";";            
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
                String question = rs.getString("question");
                String m = rs.getString("weightage");
                int o = rs.getInt("occurence");
                al10.add(question);
                al10m.add(m);
                al10o.add(o);
//                    System.out.println(al4);
            }
            if(Marks10!=0){
                if(al10.isEmpty()){
                    JOptionPane.showMessageDialog(this, "There are no enough 10 marks question from the selected chapters! Please retry");
                }else{
                    ArrayList<Integer> al10R = new ArrayList<Integer>();
                    System.out.println("Q3. 10 Marks Questions Attempt Any 4");
                    while(Marks10!=gotQ10){
                        no = random.nextInt(al10.size());
                        if(al10R.contains(no)){
                            continue;
                        }else{
                            String quest = al10.get(no);
                            String marks = al10m.get(no);
                            int occurence = al10o.get(no);
                            System.out.println(quest);
                            System.out.println(marks);
                            //start for occurence
                            int qid = 0;
                            String sqlq = "select id from question where question = '" + quest + "'";
                            try{
                                ps = conn.prepareStatement(sqlq);
                                rs = ps.executeQuery();
                                while(rs.next()){
                                    int qidtemp = rs.getInt("id");
                                    qid = qidtemp;
                                }
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            int ocdb = occurence + 1;
                            String sqloc = "update question set occurence = ? where id = '" + qid + "'";
                            try{
                                ps = conn.prepareStatement(sqloc);
                                ps.setInt(1, ocdb);
                                ps.execute();
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            //end for occurence
                            gotQ10++;
                            al10R.add(no);
                        }
                    }
                }
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
    }
    //100
    private void getQuestions100Pattern1WithoutOption(){
        //FOR CHAPTERS Name
        int i;
        String ch1 = "";
        for(i=0; i<lChapter.getSelectedValuesList().size(); i++){
            if(i!=lChapter.getSelectedValuesList().size()-1)
                ch1 += "'" + lChapter.getSelectedValuesList().get(i) + "',";
            else
                ch1 += "'" + lChapter.getSelectedValuesList().get(i) + "'";
        }
        //For chapters id
        String sel = "";
        try{
            Object oarray[] = lChapter.getSelectedValues();
            Vector v = new Vector();
            for (Object oarray1 : oarray) {
                String sqlcid = "select id from chapter where chapter_name = '" + oarray1 + "'";
                ps = conn.prepareStatement(sqlcid);
                rs = ps.executeQuery(); 
                if(rs.next())
                    v.addElement(rs.getInt(1));
            }
            int len = v.toString().length();
            sel = v.toString().substring(1,len-1);
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        //FOR SUBJECTS
        int id = 0;
        try{
            String sname = cmbSubject.getSelectedItem().toString();
            String sqlid = "Select id from subject where subject_name = '" + sname + "'";
            ps = conn.prepareStatement(sqlid);
            rs = ps.executeQuery();
            while(rs.next()){
                int idTemp = rs.getInt("id");
                id = idTemp;
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        
        String selectedDiff = cmbDiff.getSelectedItem().toString();
        String selectedSubject = cmbSubject.getSelectedItem().toString();
        int Marks4,Marks8,Marks10;
        Marks4 = 4;
        Marks8 = 3;
        Marks10 = 6;
        int no;
        int gotQ4 = 0, gotQ8 = 0, gotQ10 = 0;
        
        
        //Output
        System.out.println("Subject Name : " + selectedSubject);
        System.out.println("Chapters : " + ch1);
        System.out.println("Total Marks : 100");
        System.out.println("Duration : 3 Hours");
        //4 Marks
        try{
            ArrayList<String> al4 = new ArrayList<>();
            ArrayList<String> al4m = new ArrayList<>();
            ArrayList<Integer> al4o = new ArrayList<>();
            String sql = "select question,weightage,occurence from question where weightage = 4 and subject_id = '" + id + "' and difficulty = '" + selectedDiff + "' and chapter_id IN (" + sel + ") ORDER BY occurence LIMIT " + Marks4 + ";";            
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
                String question = rs.getString("question");
                String m = rs.getString("weightage");
                int o = rs.getInt("occurence");
                al4.add(question);
                al4m.add(m);
                al4o.add(o);
//                    System.out.println(al4);
            }
            if(Marks4!=0){
                if(al4.isEmpty()){
                    JOptionPane.showMessageDialog(this, "There are no enough 4 marks question from the selected chapters! Please retry");
                }else{
                    ArrayList<Integer> al4R = new ArrayList<Integer>();
                    System.out.println("Q1. 4 Marks Questions");
                    while(Marks4!=gotQ4){
                        no = random.nextInt(al4.size());
                        if(al4R.contains(no)){
                            continue;
                        }else{
                            String quest = al4.get(no);
                            String marks = al4m.get(no);
                            int occurence = al4o.get(no);
                            System.out.println(quest);
                            System.out.println(marks);
                            //start for occurence
                            int qid = 0;
                            String sqlq = "select id from question where question = '" + quest + "'";
                            try{
                                ps = conn.prepareStatement(sqlq);
                                rs = ps.executeQuery();
                                while(rs.next()){
                                    int qidtemp = rs.getInt("id");
                                    qid = qidtemp;
                                }
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            int ocdb = occurence + 1;
                            String sqloc = "update question set occurence = ? where id = '" + qid + "'";
                            try{
                                ps = conn.prepareStatement(sqloc);
                                ps.setInt(1, ocdb);
                                ps.execute();
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            //end for occurence
                            gotQ4++;
                            al4R.add(no);
                        }
                    }
                }
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        //8 Marks
        try{
            ArrayList<String> al8 = new ArrayList<>();
            ArrayList<String> al8m = new ArrayList<>();
            ArrayList<Integer> al8o = new ArrayList<>();
            String sql = "select question,weightage,occurence from question where weightage = 8 and subject_id = '" + id + "' and difficulty = '" + selectedDiff + "' and chapter_id IN (" + sel + ") ORDER BY occurence LIMIT " + Marks8 + ";";            
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
                String question = rs.getString("question");
                String m = rs.getString("weightage");
                int o = rs.getInt("occurence");
                al8.add(question);
                al8m.add(m);
                al8o.add(o);
//                    System.out.println(al8);
            }
            if(Marks8!=0){
                if(al8.isEmpty()){
                    JOptionPane.showMessageDialog(this, "There are no enough 8 marks question from the selected chapters! Please retry");
                }else{
                    ArrayList<Integer> al8R = new ArrayList<Integer>();
                    System.out.println("Q2. 8 Marks Questions");
                    while(Marks8!=gotQ8){
                        no = random.nextInt(al8.size());
                        if(al8R.contains(no)){
                            continue;
                        }else{
                            String quest = al8.get(no);
                            String marks = al8m.get(no);
                            int occurence = al8o.get(no);
                            System.out.println(quest);
                            System.out.println(marks);
                            //start for occurence
                            int qid = 0;
                            String sqlq = "select id from question where question = '" + quest + "'";
                            try{
                                ps = conn.prepareStatement(sqlq);
                                rs = ps.executeQuery();
                                while(rs.next()){
                                    int qidtemp = rs.getInt("id");
                                    qid = qidtemp;
                                }
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            int ocdb = occurence + 1;
                            String sqloc = "update question set occurence = ? where id = '" + qid + "'";
                            try{
                                ps = conn.prepareStatement(sqloc);
                                ps.setInt(1, ocdb);
                                ps.execute();
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            //end for occurence
                            gotQ8++;
                            al8R.add(no);
                        }
                    }
                }
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        //10 Marks
        try{
            ArrayList<String> al10 = new ArrayList<>();
            ArrayList<String> al10m = new ArrayList<>();
            ArrayList<Integer> al10o = new ArrayList<>();
            String sql = "select question,weightage,occurence from question where weightage = 10 and subject_id = '" + id + "' and difficulty = '" + selectedDiff + "' and chapter_id IN (" + sel + ") ORDER BY occurence LIMIT " + Marks10 + ";";            
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
                String question = rs.getString("question");
                String m = rs.getString("weightage");
                int o = rs.getInt("occurence");
                al10.add(question);
                al10m.add(m);
                al10o.add(o);
//                    System.out.println(al4);
            }
            if(Marks10!=0){
                if(al10.isEmpty()){
                    JOptionPane.showMessageDialog(this, "There are no enough 10 marks question from the selected chapters! Please retry");
                }else{
                    ArrayList<Integer> al10R = new ArrayList<Integer>();
                    System.out.println("Q3. 10 Marks Questions");
                    while(Marks10!=gotQ10){
                        no = random.nextInt(al10.size());
                        if(al10R.contains(no)){
                            continue;
                        }else{
                            String quest = al10.get(no);
                            String marks = al10m.get(no);
                            int occurence = al10o.get(no);
                            System.out.println(quest);
                            System.out.println(marks);
                            //start for occurence
                            int qid = 0;
                            String sqlq = "select id from question where question = '" + quest + "'";
                            try{
                                ps = conn.prepareStatement(sqlq);
                                rs = ps.executeQuery();
                                while(rs.next()){
                                    int qidtemp = rs.getInt("id");
                                    qid = qidtemp;
                                }
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            int ocdb = occurence + 1;
                            String sqloc = "update question set occurence = ? where id = '" + qid + "'";
                            try{
                                ps = conn.prepareStatement(sqloc);
                                ps.setInt(1, ocdb);
                                ps.execute();
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            //end for occurence
                            gotQ10++;
                            al10R.add(no);
                        }
                    }
                }
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
    }
    private void getQuestions100Pattern1WithOption(){
        //FOR CHAPTERS Name
        int i;
        String ch1 = "";
        for(i=0; i<lChapter.getSelectedValuesList().size(); i++){
            if(i!=lChapter.getSelectedValuesList().size()-1)
                ch1 += "'" + lChapter.getSelectedValuesList().get(i) + "',";
            else
                ch1 += "'" + lChapter.getSelectedValuesList().get(i) + "'";
        }
        //For chapters id
        String sel = "";
        try{
            Object oarray[] = lChapter.getSelectedValues();
            Vector v = new Vector();
            for (Object oarray1 : oarray) {
                String sqlcid = "select id from chapter where chapter_name = '" + oarray1 + "'";
                ps = conn.prepareStatement(sqlcid);
                rs = ps.executeQuery(); 
                if(rs.next())
                    v.addElement(rs.getInt(1));
            }
            int len = v.toString().length();
            sel = v.toString().substring(1,len-1);
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        //FOR SUBJECTS
        int id = 0;
        try{
            String sname = cmbSubject.getSelectedItem().toString();
            String sqlid = "Select id from subject where subject_name = '" + sname + "'";
            ps = conn.prepareStatement(sqlid);
            rs = ps.executeQuery();
            while(rs.next()){
                int idTemp = rs.getInt("id");
                id = idTemp;
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        
        String selectedDiff = cmbDiff.getSelectedItem().toString();
        String selectedSubject = cmbSubject.getSelectedItem().toString();
        int Marks4,Marks8,Marks10;
        Marks4 = 4 + 2;
        Marks8 = 3 + 1;
        Marks10 = 6 + 1;
        int no;
        int gotQ4 = 0, gotQ8 = 0, gotQ10 = 0;
        
        
        //Output
        System.out.println("Subject Name : " + selectedSubject);
        System.out.println("Chapters : " + ch1);
        System.out.println("Total Marks : 100");
        System.out.println("Duration : 3 Hours");
        //4 Marks
        try{
            ArrayList<String> al4 = new ArrayList<>();
            ArrayList<String> al4m = new ArrayList<>();
            ArrayList<Integer> al4o = new ArrayList<>();
            String sql = "select question,weightage,occurence from question where weightage = 4 and subject_id = '" + id + "' and difficulty = '" + selectedDiff + "' and chapter_id IN (" + sel + ") ORDER BY occurence LIMIT " + Marks4 + ";";            
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
                String question = rs.getString("question");
                String m = rs.getString("weightage");
                int o = rs.getInt("occurence");
                al4.add(question);
                al4m.add(m);
                al4o.add(o);
//                    System.out.println(al4);
            }
            if(Marks4!=0){
                if(al4.isEmpty()){
                    JOptionPane.showMessageDialog(this, "There are no enough 4 marks question from the selected chapters! Please retry");
                }else{
                    ArrayList<Integer> al4R = new ArrayList<Integer>();
                    System.out.println("Q1. 4 Marks Questions Attempt Any 4");
                    while(Marks4!=gotQ4){
                        no = random.nextInt(al4.size());
                        if(al4R.contains(no)){
                            continue;
                        }else{
                            String quest = al4.get(no);
                            String marks = al4m.get(no);
                            int occurence = al4o.get(no);
                            System.out.println(quest);
                            System.out.println(marks);
                            //start for occurence
                            int qid = 0;
                            String sqlq = "select id from question where question = '" + quest + "'";
                            try{
                                ps = conn.prepareStatement(sqlq);
                                rs = ps.executeQuery();
                                while(rs.next()){
                                    int qidtemp = rs.getInt("id");
                                    qid = qidtemp;
                                }
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            int ocdb = occurence + 1;
                            String sqloc = "update question set occurence = ? where id = '" + qid + "'";
                            try{
                                ps = conn.prepareStatement(sqloc);
                                ps.setInt(1, ocdb);
                                ps.execute();
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            //end for occurence
                            gotQ4++;
                            al4R.add(no);
                        }
                    }
                }
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        //8 Marks
        try{
            ArrayList<String> al8 = new ArrayList<>();
            ArrayList<String> al8m = new ArrayList<>();
            ArrayList<Integer> al8o = new ArrayList<>();
            String sql = "select question,weightage,occurence from question where weightage = 8 and subject_id = '" + id + "' and difficulty = '" + selectedDiff + "' and chapter_id IN (" + sel + ") ORDER BY occurence LIMIT " + Marks8 + ";";            
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
                String question = rs.getString("question");
                String m = rs.getString("weightage");
                int o = rs.getInt("occurence");
                al8.add(question);
                al8m.add(m);
                al8o.add(o);
//                    System.out.println(al8);
            }
            if(Marks8!=0){
                if(al8.isEmpty()){
                    JOptionPane.showMessageDialog(this, "There are no enough 8 marks question from the selected chapters! Please retry");
                }else{
                    ArrayList<Integer> al8R = new ArrayList<Integer>();
                    System.out.println("Q2. 8 Marks Questions Attempt Any 3");
                    while(Marks8!=gotQ8){
                        no = random.nextInt(al8.size());
                        if(al8R.contains(no)){
                            continue;
                        }else{
                            String quest = al8.get(no);
                            String marks = al8m.get(no);
                            int occurence = al8o.get(no);
                            System.out.println(quest);
                            System.out.println(marks);
                            //start for occurence
                            int qid = 0;
                            String sqlq = "select id from question where question = '" + quest + "'";
                            try{
                                ps = conn.prepareStatement(sqlq);
                                rs = ps.executeQuery();
                                while(rs.next()){
                                    int qidtemp = rs.getInt("id");
                                    qid = qidtemp;
                                }
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            int ocdb = occurence + 1;
                            String sqloc = "update question set occurence = ? where id = '" + qid + "'";
                            try{
                                ps = conn.prepareStatement(sqloc);
                                ps.setInt(1, ocdb);
                                ps.execute();
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            //end for occurence
                            gotQ8++;
                            al8R.add(no);
                        }
                    }
                }
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        //10 Marks
        try{
            ArrayList<String> al10 = new ArrayList<>();
            ArrayList<String> al10m = new ArrayList<>();
            ArrayList<Integer> al10o = new ArrayList<>();
            String sql = "select question,weightage,occurence from question where weightage = 10 and subject_id = '" + id + "' and difficulty = '" + selectedDiff + "' and chapter_id IN (" + sel + ") ORDER BY occurence LIMIT " + Marks10 + ";";            
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
                String question = rs.getString("question");
                String m = rs.getString("weightage");
                int o = rs.getInt("occurence");
                al10.add(question);
                al10m.add(m);
                al10o.add(o);
//                    System.out.println(al4);
            }
            if(Marks10!=0){
                if(al10.isEmpty()){
                    JOptionPane.showMessageDialog(this, "There are no enough 10 marks question from the selected chapters! Please retry");
                }else{
                    ArrayList<Integer> al10R = new ArrayList<Integer>();
                    System.out.println("Q3. 10 Marks Questions Attempt Any 6");
                    while(Marks10!=gotQ10){
                        no = random.nextInt(al10.size());
                        if(al10R.contains(no)){
                            continue;
                        }else{
                            String quest = al10.get(no);
                            String marks = al10m.get(no);
                            int occurence = al10o.get(no);
                            System.out.println(quest);
                            System.out.println(marks);
                            //start for occurence
                            int qid = 0;
                            String sqlq = "select id from question where question = '" + quest + "'";
                            try{
                                ps = conn.prepareStatement(sqlq);
                                rs = ps.executeQuery();
                                while(rs.next()){
                                    int qidtemp = rs.getInt("id");
                                    qid = qidtemp;
                                }
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            int ocdb = occurence + 1;
                            String sqloc = "update question set occurence = ? where id = '" + qid + "'";
                            try{
                                ps = conn.prepareStatement(sqloc);
                                ps.setInt(1, ocdb);
                                ps.execute();
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            //end for occurence
                            gotQ10++;
                            al10R.add(no);
                        }
                    }
                }
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
    }
    private void getQuestions100Pattern2WithoutOption(){
        //FOR CHAPTERS Name
        int i;
        String ch1 = "";
        for(i=0; i<lChapter.getSelectedValuesList().size(); i++){
            if(i!=lChapter.getSelectedValuesList().size()-1)
                ch1 += "'" + lChapter.getSelectedValuesList().get(i) + "',";
            else
                ch1 += "'" + lChapter.getSelectedValuesList().get(i) + "'";
        }
        //For chapters id
        String sel = "";
        try{
            Object oarray[] = lChapter.getSelectedValues();
            Vector v = new Vector();
            for (Object oarray1 : oarray) {
                String sqlcid = "select id from chapter where chapter_name = '" + oarray1 + "'";
                ps = conn.prepareStatement(sqlcid);
                rs = ps.executeQuery(); 
                if(rs.next())
                    v.addElement(rs.getInt(1));
            }
            int len = v.toString().length();
            sel = v.toString().substring(1,len-1);
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        //FOR SUBJECTS
        int id = 0;
        try{
            String sname = cmbSubject.getSelectedItem().toString();
            String sqlid = "Select id from subject where subject_name = '" + sname + "'";
            ps = conn.prepareStatement(sqlid);
            rs = ps.executeQuery();
            while(rs.next()){
                int idTemp = rs.getInt("id");
                id = idTemp;
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        
        String selectedDiff = cmbDiff.getSelectedItem().toString();
        String selectedSubject = cmbSubject.getSelectedItem().toString();
        int Marks4,Marks8,Marks10;
        Marks4 = 2;
        Marks8 = 4;
        Marks10 = 6;
        int no;
        int gotQ4 = 0, gotQ8 = 0, gotQ10 = 0;
        
        
        //Output
        System.out.println("Subject Name : " + selectedSubject);
        System.out.println("Chapters : " + ch1);
        System.out.println("Total Marks : 100");
        System.out.println("Duration : 3 Hours");
        //4 Marks
        try{
            ArrayList<String> al4 = new ArrayList<>();
            ArrayList<String> al4m = new ArrayList<>();
            ArrayList<Integer> al4o = new ArrayList<>();
            String sql = "select question,weightage,occurence from question where weightage = 4 and subject_id = '" + id + "' and difficulty = '" + selectedDiff + "' and chapter_id IN (" + sel + ") ORDER BY occurence LIMIT " + Marks4 + ";";            
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
                String question = rs.getString("question");
                String m = rs.getString("weightage");
                int o = rs.getInt("occurence");
                al4.add(question);
                al4m.add(m);
                al4o.add(o);
//                    System.out.println(al4);
            }
            if(Marks4!=0){
                if(al4.isEmpty()){
                    JOptionPane.showMessageDialog(this, "There are no enough 4 marks question from the selected chapters! Please retry");
                }else{
                    ArrayList<Integer> al4R = new ArrayList<Integer>();
                    System.out.println("Q1. 4 Marks Questions");
                    while(Marks4!=gotQ4){
                        no = random.nextInt(al4.size());
                        if(al4R.contains(no)){
                            continue;
                        }else{
                            String quest = al4.get(no);
                            String marks = al4m.get(no);
                            int occurence = al4o.get(no);
                            System.out.println(quest);
                            System.out.println(marks);
                            //start for occurence
                            int qid = 0;
                            String sqlq = "select id from question where question = '" + quest + "'";
                            try{
                                ps = conn.prepareStatement(sqlq);
                                rs = ps.executeQuery();
                                while(rs.next()){
                                    int qidtemp = rs.getInt("id");
                                    qid = qidtemp;
                                }
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            int ocdb = occurence + 1;
                            String sqloc = "update question set occurence = ? where id = '" + qid + "'";
                            try{
                                ps = conn.prepareStatement(sqloc);
                                ps.setInt(1, ocdb);
                                ps.execute();
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            //end for occurence
                            gotQ4++;
                            al4R.add(no);
                        }
                    }
                }
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        //8 Marks
        try{
            ArrayList<String> al8 = new ArrayList<>();
            ArrayList<String> al8m = new ArrayList<>();
            ArrayList<Integer> al8o = new ArrayList<>();
            String sql = "select question,weightage,occurence from question where weightage = 8 and subject_id = '" + id + "' and difficulty = '" + selectedDiff + "' and chapter_id IN (" + sel + ") ORDER BY occurence LIMIT " + Marks8 + ";";            
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
                String question = rs.getString("question");
                String m = rs.getString("weightage");
                int o = rs.getInt("occurence");
                al8.add(question);
                al8m.add(m);
                al8o.add(o);
//                    System.out.println(al8);
            }
            if(Marks8!=0){
                if(al8.isEmpty()){
                    JOptionPane.showMessageDialog(this, "There are no enough 8 marks question from the selected chapters! Please retry");
                }else{
                    ArrayList<Integer> al8R = new ArrayList<Integer>();
                    System.out.println("Q2. 8 Marks Questions");
                    while(Marks8!=gotQ8){
                        no = random.nextInt(al8.size());
                        if(al8R.contains(no)){
                            continue;
                        }else{
                            String quest = al8.get(no);
                            String marks = al8m.get(no);
                            int occurence = al8o.get(no);
                            System.out.println(quest);
                            System.out.println(marks);
                            //start for occurence
                            int qid = 0;
                            String sqlq = "select id from question where question = '" + quest + "'";
                            try{
                                ps = conn.prepareStatement(sqlq);
                                rs = ps.executeQuery();
                                while(rs.next()){
                                    int qidtemp = rs.getInt("id");
                                    qid = qidtemp;
                                }
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            int ocdb = occurence + 1;
                            String sqloc = "update question set occurence = ? where id = '" + qid + "'";
                            try{
                                ps = conn.prepareStatement(sqloc);
                                ps.setInt(1, ocdb);
                                ps.execute();
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            //end for occurence
                            gotQ8++;
                            al8R.add(no);
                        }
                    }
                }
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        //10 Marks
        try{
            ArrayList<String> al10 = new ArrayList<>();
            ArrayList<String> al10m = new ArrayList<>();
            ArrayList<Integer> al10o = new ArrayList<>();
            String sql = "select question,weightage,occurence from question where weightage = 10 and subject_id = '" + id + "' and difficulty = '" + selectedDiff + "' and chapter_id IN (" + sel + ") ORDER BY occurence LIMIT " + Marks10 + ";";            
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
                String question = rs.getString("question");
                String m = rs.getString("weightage");
                int o = rs.getInt("occurence");
                al10.add(question);
                al10m.add(m);
                al10o.add(o);
//                    System.out.println(al4);
            }
            if(Marks10!=0){
                if(al10.isEmpty()){
                    JOptionPane.showMessageDialog(this, "There are no enough 10 marks question from the selected chapters! Please retry");
                }else{
                    ArrayList<Integer> al10R = new ArrayList<Integer>();
                    System.out.println("Q3. 10 Marks Questions");
                    while(Marks10!=gotQ10){
                        no = random.nextInt(al10.size());
                        if(al10R.contains(no)){
                            continue;
                        }else{
                            String quest = al10.get(no);
                            String marks = al10m.get(no);
                            int occurence = al10o.get(no);
                            System.out.println(quest);
                            System.out.println(marks);
                            //start for occurence
                            int qid = 0;
                            String sqlq = "select id from question where question = '" + quest + "'";
                            try{
                                ps = conn.prepareStatement(sqlq);
                                rs = ps.executeQuery();
                                while(rs.next()){
                                    int qidtemp = rs.getInt("id");
                                    qid = qidtemp;
                                }
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            int ocdb = occurence + 1;
                            String sqloc = "update question set occurence = ? where id = '" + qid + "'";
                            try{
                                ps = conn.prepareStatement(sqloc);
                                ps.setInt(1, ocdb);
                                ps.execute();
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            //end for occurence
                            gotQ10++;
                            al10R.add(no);
                        }
                    }
                }
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
    } 
    private void getQuestions100Pattern2WithOption(){
        //FOR CHAPTERS Name
        int i;
        String ch1 = "";
        for(i=0; i<lChapter.getSelectedValuesList().size(); i++){
            if(i!=lChapter.getSelectedValuesList().size()-1)
                ch1 += "'" + lChapter.getSelectedValuesList().get(i) + "',";
            else
                ch1 += "'" + lChapter.getSelectedValuesList().get(i) + "'";
        }
        //For chapters id
        String sel = "";
        try{
            Object oarray[] = lChapter.getSelectedValues();
            Vector v = new Vector();
            for (Object oarray1 : oarray) {
                String sqlcid = "select id from chapter where chapter_name = '" + oarray1 + "'";
                ps = conn.prepareStatement(sqlcid);
                rs = ps.executeQuery(); 
                if(rs.next())
                    v.addElement(rs.getInt(1));
            }
            int len = v.toString().length();
            sel = v.toString().substring(1,len-1);
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        //FOR SUBJECTS
        int id = 0;
        try{
            String sname = cmbSubject.getSelectedItem().toString();
            String sqlid = "Select id from subject where subject_name = '" + sname + "'";
            ps = conn.prepareStatement(sqlid);
            rs = ps.executeQuery();
            while(rs.next()){
                int idTemp = rs.getInt("id");
                id = idTemp;
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        
        String selectedDiff = cmbDiff.getSelectedItem().toString();
        String selectedSubject = cmbSubject.getSelectedItem().toString();
        int Marks4,Marks8,Marks10;
        Marks4 = 2 + 1;
        Marks8 = 4 + 2;
        Marks10 = 6 + 2;
        int no;
        int gotQ4 = 0, gotQ8 = 0, gotQ10 = 0;
        
        
        //Output
        System.out.println("Subject Name : " + selectedSubject);
        System.out.println("Chapters : " + ch1);
        System.out.println("Total Marks : 100");
        System.out.println("Duration : 3 Hours");
        //4 Marks
        try{
            ArrayList<String> al4 = new ArrayList<>();
            ArrayList<String> al4m = new ArrayList<>();
            ArrayList<Integer> al4o = new ArrayList<>();
            String sql = "select question,weightage,occurence from question where weightage = 4 and subject_id = '" + id + "' and difficulty = '" + selectedDiff + "' and chapter_id IN (" + sel + ") ORDER BY occurence LIMIT " + Marks4 + ";";            
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
                String question = rs.getString("question");
                String m = rs.getString("weightage");
                int o = rs.getInt("occurence");
                al4.add(question);
                al4m.add(m);
                al4o.add(o);
//                    System.out.println(al4);
            }
            if(Marks4!=0){
                if(al4.isEmpty()){
                    JOptionPane.showMessageDialog(this, "There are no enough 4 marks question from the selected chapters! Please retry");
                }else{
                    ArrayList<Integer> al4R = new ArrayList<Integer>();
                    System.out.println("Q1. 4 Marks Questions Attempt Any 2");
                    while(Marks4!=gotQ4){
                        no = random.nextInt(al4.size());
                        if(al4R.contains(no)){
                            continue;
                        }else{
                            String quest = al4.get(no);
                            String marks = al4m.get(no);
                            int occurence = al4o.get(no);
                            System.out.println(quest);
                            System.out.println(marks);
                            //start for occurence
                            int qid = 0;
                            String sqlq = "select id from question where question = '" + quest + "'";
                            try{
                                ps = conn.prepareStatement(sqlq);
                                rs = ps.executeQuery();
                                while(rs.next()){
                                    int qidtemp = rs.getInt("id");
                                    qid = qidtemp;
                                }
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            int ocdb = occurence + 1;
                            String sqloc = "update question set occurence = ? where id = '" + qid + "'";
                            try{
                                ps = conn.prepareStatement(sqloc);
                                ps.setInt(1, ocdb);
                                ps.execute();
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            //end for occurence
                            gotQ4++;
                            al4R.add(no);
                        }
                    }
                }
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        //8 Marks
        try{
            ArrayList<String> al8 = new ArrayList<>();
            ArrayList<String> al8m = new ArrayList<>();
            ArrayList<Integer> al8o = new ArrayList<>();
            String sql = "select question,weightage,occurence from question where weightage = 8 and subject_id = '" + id + "' and difficulty = '" + selectedDiff + "' and chapter_id IN (" + sel + ") ORDER BY occurence LIMIT " + Marks8 + ";";            
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
                String question = rs.getString("question");
                String m = rs.getString("weightage");
                int o = rs.getInt("occurence");
                al8.add(question);
                al8m.add(m);
                al8o.add(o);
//                    System.out.println(al8);
            }
            if(Marks8!=0){
                if(al8.isEmpty()){
                    JOptionPane.showMessageDialog(this, "There are no enough 8 marks question from the selected chapters! Please retry");
                }else{
                    ArrayList<Integer> al8R = new ArrayList<Integer>();
                    System.out.println("Q2. 8 Marks Questions Attempt Any 4");
                    while(Marks8!=gotQ8){
                        no = random.nextInt(al8.size());
                        if(al8R.contains(no)){
                            continue;
                        }else{
                            String quest = al8.get(no);
                            String marks = al8m.get(no);
                            int occurence = al8o.get(no);
                            System.out.println(quest);
                            System.out.println(marks);
                            //start for occurence
                            int qid = 0;
                            String sqlq = "select id from question where question = '" + quest + "'";
                            try{
                                ps = conn.prepareStatement(sqlq);
                                rs = ps.executeQuery();
                                while(rs.next()){
                                    int qidtemp = rs.getInt("id");
                                    qid = qidtemp;
                                }
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            int ocdb = occurence + 1;
                            String sqloc = "update question set occurence = ? where id = '" + qid + "'";
                            try{
                                ps = conn.prepareStatement(sqloc);
                                ps.setInt(1, ocdb);
                                ps.execute();
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            //end for occurence
                            gotQ8++;
                            al8R.add(no);
                        }
                    }
                }
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
        //10 Marks
        try{
            ArrayList<String> al10 = new ArrayList<>();
            ArrayList<String> al10m = new ArrayList<>();
            ArrayList<Integer> al10o = new ArrayList<>();
            String sql = "select question,weightage,occurence from question where weightage = 10 and subject_id = '" + id + "' and difficulty = '" + selectedDiff + "' and chapter_id IN (" + sel + ") ORDER BY occurence LIMIT " + Marks10 + ";";            
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
                String question = rs.getString("question");
                String m = rs.getString("weightage");
                int o = rs.getInt("occurence");
                al10.add(question);
                al10m.add(m);
                al10o.add(o);
//                    System.out.println(al4);
            }
            if(Marks10!=0){
                if(al10.isEmpty()){
                    JOptionPane.showMessageDialog(this, "There are no enough 10 marks question from the selected chapters! Please retry");
                }else{
                    ArrayList<Integer> al10R = new ArrayList<Integer>();
                    System.out.println("Q3. 10 Marks Questions Attempt Any 6");
                    while(Marks10!=gotQ10){
                        no = random.nextInt(al10.size());
                        if(al10R.contains(no)){
                            continue;
                        }else{
                            String quest = al10.get(no);
                            String marks = al10m.get(no);
                            int occurence = al10o.get(no);
                            System.out.println(quest);
                            System.out.println(marks);
                            //start for occurence
                            int qid = 0;
                            String sqlq = "select id from question where question = '" + quest + "'";
                            try{
                                ps = conn.prepareStatement(sqlq);
                                rs = ps.executeQuery();
                                while(rs.next()){
                                    int qidtemp = rs.getInt("id");
                                    qid = qidtemp;
                                }
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            int ocdb = occurence + 1;
                            String sqloc = "update question set occurence = ? where id = '" + qid + "'";
                            try{
                                ps = conn.prepareStatement(sqloc);
                                ps.setInt(1, ocdb);
                                ps.execute();
                            }catch(Exception e){
                                JOptionPane.showMessageDialog(this, "Error! " + e);
                            }
                            //end for occurence
                            gotQ10++;
                            al10R.add(no);
                        }
                    }
                }
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        rbtTemp = new javax.swing.JRadioButton();
        rbtGroup = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        cmbSubject = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        lChapter = new javax.swing.JList<>();
        cmbDiff = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();
        rbtYes = new javax.swing.JRadioButton();
        rbtNo = new javax.swing.JRadioButton();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        cmb40Marks = new javax.swing.JComboBox<>();
        cmb80Marks = new javax.swing.JComboBox<>();
        cmb100Marks = new javax.swing.JComboBox<>();
        btnSubmit = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();

        rbtGroup.add(rbtTemp);
        rbtTemp.setText("jRadioButton3");

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Generate Paper PreDefined", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Monospaced", 1, 24))); // NOI18N

        jLabel1.setFont(new java.awt.Font("Monospaced", 1, 18)); // NOI18N
        jLabel1.setText("Subject");

        jLabel2.setFont(new java.awt.Font("Monospaced", 1, 18)); // NOI18N
        jLabel2.setText("Chapter");

        jLabel3.setFont(new java.awt.Font("Monospaced", 1, 18)); // NOI18N
        jLabel3.setText("Difficulty");

        cmbSubject.setFont(new java.awt.Font("Monospaced", 0, 18)); // NOI18N

        lChapter.setFont(new java.awt.Font("Monospaced", 0, 18)); // NOI18N
        lChapter.setMaximumSize(new java.awt.Dimension(140, 140));
        jScrollPane1.setViewportView(lChapter);

        cmbDiff.setFont(new java.awt.Font("Monospaced", 0, 18)); // NOI18N
        cmbDiff.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Easy", "Medium", "Hard" }));

        jLabel5.setFont(new java.awt.Font("Monospaced", 1, 18)); // NOI18N
        jLabel5.setText("Options");

        rbtGroup.add(rbtYes);
        rbtYes.setFont(new java.awt.Font("Monospaced", 0, 18)); // NOI18N
        rbtYes.setText("Yes");
        rbtYes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtYesActionPerformed(evt);
            }
        });

        rbtGroup.add(rbtNo);
        rbtNo.setFont(new java.awt.Font("Monospaced", 0, 18)); // NOI18N
        rbtNo.setText("No");
        rbtNo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtNoActionPerformed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Monospaced", 1, 18)); // NOI18N
        jLabel6.setText("40 Marks");

        jLabel7.setFont(new java.awt.Font("Monospaced", 1, 18)); // NOI18N
        jLabel7.setText("80 Marks");

        jLabel8.setFont(new java.awt.Font("Monospaced", 1, 18)); // NOI18N
        jLabel8.setText("100 Marks");

        cmb40Marks.setFont(new java.awt.Font("Monospaced", 0, 18)); // NOI18N
        cmb40Marks.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "4M3Q,8M1Q,10M2Q", "4M1Q,8M2Q,10M2Q" }));
        cmb40Marks.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmb40MarksItemStateChanged(evt);
            }
        });
        cmb40Marks.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmb40MarksActionPerformed(evt);
            }
        });

        cmb80Marks.setFont(new java.awt.Font("Monospaced", 0, 18)); // NOI18N
        cmb80Marks.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "4M6Q,8M2Q,10M4Q", "4M2Q,8M4Q,10M4Q" }));
        cmb80Marks.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmb80MarksItemStateChanged(evt);
            }
        });
        cmb80Marks.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmb80MarksActionPerformed(evt);
            }
        });

        cmb100Marks.setFont(new java.awt.Font("Monospaced", 0, 18)); // NOI18N
        cmb100Marks.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "4M4Q,8M3Q,10M6Q", "4M2Q,8M4Q,10M6Q" }));
        cmb100Marks.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmb100MarksActionPerformed(evt);
            }
        });

        btnSubmit.setFont(new java.awt.Font("Monospaced", 1, 24)); // NOI18N
        btnSubmit.setText("Submit");
        btnSubmit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSubmitActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Monospaced", 1, 18)); // NOI18N
        jLabel4.setText("Note : Sufficient Questions Should Be Inserted.");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnSubmit)
                .addGap(369, 369, 369))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel1)
                                        .addGap(18, 18, 18)
                                        .addComponent(cmbSubject, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(49, 49, 49)
                                        .addComponent(jLabel2))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(116, 116, 116)
                                        .addComponent(jLabel6)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(49, 49, 49)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel3)
                                            .addComponent(jLabel5)))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel7)
                                        .addGap(186, 186, 186))))
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(rbtYes)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 28, Short.MAX_VALUE)
                                .addComponent(rbtNo))
                            .addComponent(cmbDiff, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(17, 17, 17)
                                .addComponent(jLabel8)))
                        .addContainerGap(53, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(64, 64, 64)
                        .addComponent(cmb40Marks, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(78, 78, 78)
                        .addComponent(cmb80Marks, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cmb100Marks, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(30, 30, 30))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(46, 46, 46)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel3)
                        .addComponent(cmbDiff, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)
                            .addComponent(cmbSubject, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(54, 54, 54)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(rbtYes)
                            .addComponent(rbtNo))))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(jLabel6)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmb40Marks, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmb80Marks, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmb100Marks, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(39, 39, 39)
                .addComponent(btnSubmit)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 19, Short.MAX_VALUE)
                .addComponent(jLabel4)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnSubmitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSubmitActionPerformed
        // TODO add your handling code here:
        //40 Marks
        if(operation == 1){
            String selectedPattern = cmb40Marks.getSelectedItem().toString();
            if(selectedPattern.equalsIgnoreCase("4M3Q,8M1Q,10M2Q")){
                if(option.equalsIgnoreCase("No")){
                    getQuestions40Pattern1WithoutOption();
                }else{
                    getQuestions40Pattern1WithOption();
                }
            }else if(selectedPattern.equalsIgnoreCase("4M1Q,8M2Q,10M2Q")){
                if(option.equalsIgnoreCase("No")){
                    getQuestions40Pattern2WithoutOption();
                }else{
                    getQuestions40Pattern2WithOption();
                }
            }
        }
        
        //80 Marks
        if(operation == 2){
            String selectedPattern1 = cmb80Marks.getSelectedItem().toString();
            if(selectedPattern1.equalsIgnoreCase("4M6Q,8M2Q,10M4Q")){
                if(option.equalsIgnoreCase("No")){
                    getQuestions80Pattern1WithoutOption();
                }else{
                    getQuestions80Pattern1WithOption();
                }
            }else if(selectedPattern1.equalsIgnoreCase("4M2Q,8M4Q,10M4Q")){
                if(option.equalsIgnoreCase("No")){
                    getQuestions80Pattern2WithoutOption();
                }else{
                    getQuestions80Pattern2WithOption();
                }
            }
        }
//        
//        //100 Marks
        if(operation == 3){
            String selectedPattern2 = cmb100Marks.getSelectedItem().toString();
            if(selectedPattern2.equalsIgnoreCase("4M4Q,8M3Q,10M6Q")){
                if(option.equalsIgnoreCase("No")){
                    getQuestions100Pattern1WithoutOption();
                }else{
                    getQuestions100Pattern1WithOption();
                }
            }else if(selectedPattern2.equalsIgnoreCase("4M2Q,8M4Q,10M6Q")){
                if(option.equalsIgnoreCase("No")){
                    getQuestions100Pattern2WithoutOption();
                }else{
                    getQuestions100Pattern2WithOption();
                }
            }
        }
        
    }//GEN-LAST:event_btnSubmitActionPerformed

    private void rbtYesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtYesActionPerformed
        // TODO add your handling code here:
        option = "Yes";
    }//GEN-LAST:event_rbtYesActionPerformed

    private void rbtNoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtNoActionPerformed
        // TODO add your handling code here:
        option = "No";
    }//GEN-LAST:event_rbtNoActionPerformed

    private void cmb40MarksItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmb40MarksItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cmb40MarksItemStateChanged

    private void cmb80MarksItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmb80MarksItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cmb80MarksItemStateChanged

    private void cmb100MarksActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmb100MarksActionPerformed
        // TODO add your handling code here:
        operation = 3;
    }//GEN-LAST:event_cmb100MarksActionPerformed

    private void cmb40MarksActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmb40MarksActionPerformed
        // TODO add your handling code here:
        operation = 1;
    }//GEN-LAST:event_cmb40MarksActionPerformed

    private void cmb80MarksActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmb80MarksActionPerformed
        // TODO add your handling code here:
        operation = 2;
    }//GEN-LAST:event_cmb80MarksActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSubmit;
    private javax.swing.JComboBox<String> cmb100Marks;
    private javax.swing.JComboBox<String> cmb40Marks;
    private javax.swing.JComboBox<String> cmb80Marks;
    private javax.swing.JComboBox<String> cmbDiff;
    private javax.swing.JComboBox<String> cmbSubject;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JList<String> lChapter;
    private javax.swing.ButtonGroup rbtGroup;
    private javax.swing.JRadioButton rbtNo;
    private javax.swing.JRadioButton rbtTemp;
    private javax.swing.JRadioButton rbtYes;
    // End of variables declaration//GEN-END:variables
}
